# -*- coding: utf-8 -*-
"""
Created on Tue Jul  9 07:17:08 2019

@author: s8570974
"""
import os, sys
import numpy as np
import cv2
from matplotlib.pyplot import cm
from imageio import get_writer#, get_reader
from tqdm import tqdm
import traceback
#import pymsgbox

def extend_img(img, macro_block_size = 16):
    """Add black pixels at right and bottom border of image, so that 
    xy-size % macro_block_size = 0
    
    Parameters:
        img: *ndarray*
            2D or 3D np.array of image pixel values
        macro_block_size: *int*
            Integer value that image.size will be made dividable to
            
    out: *ndarray*
        img + black pixels at right and bottom border
    """
    if len(img.shape) == 2:
        x_size = img[0,:].size
        y_size = img[:,0].size
    if len(img.shape) == 3:
        x_size = img[0,:,0].size
        y_size = img[:,0,0].size    
     
    if x_size % macro_block_size == 0:
        right_add = 0
    else:
        right_add = ((x_size // macro_block_size + 1) * macro_block_size
                      - x_size)

    if y_size % macro_block_size == 0:
        bottom_add = 0
    else:
        bottom_add = ((y_size // macro_block_size + 1) * macro_block_size
                      - y_size)
 
    frame = cv2.copyMakeBorder(img, 0, bottom_add, 0, right_add, 
                               borderType = cv2.BORDER_CONSTANT)
    
    return frame

def VideoWriteSepObjects(data_set, DATA, FirstFrame=0, LastFrame=0,
                         save_vidfile = "Videos\\sepCells_avi.avi",
                         macro_block_size=16):
    """Write video showing which object number was assigned to each detected 
    event.
    Parameters:
        data_set: *dclab.dataset*
            dclab dataset that was analyzed.
        DATA: *dictionary*
            Dictionary containing the object information for each videoframe
        FirstFrame: *int, optional*
            First frame in vidfile to analyze. Default is FirstFrame = 0.
        LastFrame: *int, optional*
            Last frame in vidfile to analyze. If LastFrame = 0 (default) all 
            video frames will be analyzed
        save_vidfile: *string, optional*
            File path where finished video will be saved. If not specified, the
            default path is ".\Offline\test_avi.avi" 
    """

    if save_vidfile == "Videos\\sepCells_avi.avi":
        if not os.path.isdir("Videos"):
            os.makedirs("Videos")
    
    else:
        viddir = "\\".join(save_vidfile.split("\\")[:-1])
        if not os.path.isdir(viddir):
            raise IOError("Directory \"" + viddir + "\" not found. Please " 
                          "create folder first")
            
    vidwriter = get_writer(save_vidfile, fps=60, quality = 8,
                                   macro_block_size = macro_block_size)
    
    #default colors of matplotlib. * 255 to convert to uint8 for use with cv2
    colors = np.array(cm.get_cmap('tab10').colors) * 255
    contourData = np.array(DATA['contour_raw'])
    object_index = DATA['object index']
    object_index = np.array(object_index, dtype = int)
    Pos_x = np.array(DATA['pos_x'])
    Pos_y = np.array(DATA['pos_y'])
    font = cv2.FONT_HERSHEY_SIMPLEX

    if LastFrame == 0 or LastFrame > len(data_set['frame']):
        LastFrame = len(data_set['frame'])
    if FirstFrame < 0:
        FirstFrame = 0
        
    #don't use duplicates of frames in video --> unique frames only
    frames = data_set['frame'][FirstFrame:LastFrame]
    frames_uniq, index_uniq_frames = np.unique(frames, return_index=True)

    for ii in index_uniq_frames:
        #get image from dataset
        image = data_set['image'][ii]  
        image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)

        frame = data_set['frame'][ii]
        #if the frame number is in the filtered/tracked data: draw the contour
        if frame in DATA['frame']:
            #find index of frame in DATA
            frame_ind = np.nonzero(DATA['frame'] == frame)[0]
    
            #parameters of events found in this frame 
            contours = contourData[frame_ind]
            objects = object_index[frame_ind]
            pos_x = Pos_x[frame_ind]
            pos_y = Pos_y[frame_ind]
    
            #write video to see how detection worked out
            if len(frame_ind)>0:
                for cont in range(len(frame_ind)):
                    #get object index
                    obj_ind = objects[cont]
    
                    #get contour object in format suited for cv2.drawContours
                    #dtype=int necessary for data from tdms files
                    cont_ii = [np.array(contours[cont], dtype=int)]
                    #draw closed colored contour around object
                    cv2.drawContours(image, cont_ii, -1,
                                     colors[obj_ind % len(colors)], -1)
                    #write object index above object
                    cv2.putText(image, str(obj_ind), 
                                (int(pos_x[cont])-5,int(pos_y[cont])),
                                font, 0.5, (255,255,255),2,cv2.LINE_AA)
        
        #extend image with black pixels, so each dimension is dividable by
        #macro_block_size; needed for vid codecs to work correctly
        image = extend_img(image, macro_block_size = macro_block_size)
        vidwriter.append_data(image)

    vidwriter.close()

def VideoWriteSepObjects_dclab(data_set, data_set_filtered, DATA,
                               FirstFrame=0, LastFrame=0,
                               save_vidfile = "Videos\\sepCells_avi.avi",
                               macro_block_size=16, 
                               drawZones=False, **kws):
    """Write video showing which object number was assigned to each detected 
    event. It is necessary to have filtered and unfiltered dataset as input 
    because video images should be from unfiltered dataset while contours 
    should be taken from the filtered dataset. If drawZones=True, additionally
    the zones where the objects were predicted for each frame are drawn. (Nees
    additional argument `ZONES=`)
    
    Parameters:
        data_set: *dclab.dataset*
            UNFILTERED dclab dataset that was analyzed.
        data_set_filtered: *dclab.dataset*
            FILTERED dclab dataset that was analyzed.
        DATA: *dictionary*
            Dictionary containing the object information for each videoframe
        zones: *dictionary*
            Dictionary generated in separate_objects that contains the 
            prediction zones for the objects in each frame
        FirstFrame: *int, optional*
            First frame in vidfile to analyze. Default is FirstFrame = 0.
        LastFrame: *int, optional*
            Last frame in vidfile to analyze. If LastFrame = 0 (default) all 
            video frames will be analyzed
        save_vidfile: *string, optional*
            File path where finished video will be saved. If not specified, the
            default path is ".\Offline\test_avi.avi" 
        macro_block_size: *int*
            Parameter that is used in ffmpeg for video coding. 
            macro_block_size=16 indicates that every image dimension is 
            dividable by 16. The images will be adapted to this size by adding
            black pixels.
            h264 ad h265 codec need macro_block_size=16.
        drawZones: *bool*
            If True then the zones where an object is predicted are drawn. 
            Needs the prediction zones as ZONES parameter from the 
            `seperate_objects` function.
    """
    PIX_SIZE = data_set.config['imaging']['pixel size']
    
    if drawZones and not 'ZONES' in kws:
        raise IOError("If drawZones=True then prediction zones must be passed "
                      + "by ZONES= ... ")
        sys.exit()
    if drawZones and 'ZONES' in kws:
        ZONES = kws['ZONES']
        
    if save_vidfile == "Videos\\sepCells_avi.avi":
        if not os.path.isdir("Videos"):
            os.makedirs("Videos")
    
    else:
        viddir = "\\".join(save_vidfile.split("\\")[:-1])
        if not os.path.isdir(viddir):
            raise IOError("Directory \"" + viddir + "\" not found. Please " 
                          "create folder first")
            sys.exit()
            
    vidwriter = get_writer(save_vidfile, fps=60, quality = 8,
                                   macro_block_size = macro_block_size)
    
    #default colors of matplotlib. * 255 to convert to uint8 for use with cv2
    colors = np.array(cm.get_cmap('tab10').colors) * 255
#    contourData = np.array(DATA['contour_raw'])
    object_index = DATA['object index']
    object_index = np.array(object_index, dtype = int)
    Pos_x = np.array(DATA['pos_x'])
    Pos_y = np.array(DATA['pos_y'])
    Pos_x_um = np.array(DATA['pos_x_um'])

    font = cv2.FONT_HERSHEY_SIMPLEX

    if LastFrame == 0 or LastFrame > len(data_set['frame']):
        LastFrame = len(data_set['frame'])
    if FirstFrame < 0:
        FirstFrame = 0
        
    #don't use duplicates of frames in video --> unique frames only
    frames = data_set['frame'][FirstFrame:LastFrame]
    frames_uniq, index_uniq_frames = np.unique(frames, return_index=True)
    #add offset from FirstFrame to unique indices
    index_uniq_frames += FirstFrame

    print("\nWriting video")
    for ii in tqdm(index_uniq_frames):
        try:
            #get image from dataset
            image = data_set['image'][ii]  
            image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)
    
            frame = data_set['frame'][ii]
            
            if drawZones:
                image_out = image.copy()
                if str(frame) in ZONES.keys():
                    zones = ZONES[str(frame)]
                    for zone in zones:
                        zone_obj = zone[0]
                        if not (zone[1][0] == 'nan' or zone[1][1] == 'nan'):
                            zone_start = int(zone[1][0] / PIX_SIZE)
                            zone_end = int(zone[1][1] / PIX_SIZE)
                            h = image_out.shape[0]
                            
                            pt1 = (zone_start,0)
                            pt2 = (zone_end,h)
                            c = colors[zone_obj % len(colors)]
                            cv2.rectangle(image_out, pt1, pt2, c, -1)
            
            #if the frame number is in the filtered/tracked data: draw the 
            #contour
            if frame in DATA['frame']:
                #find index of frame in DATA
                frame_ind = np.nonzero(DATA['frame'] == frame)[0]
                #parameters of events found in this frame 
    #            contours = data_set['contour'][frame_ind]
                objects = object_index[frame_ind]
                pos_x = Pos_x[frame_ind]
                pos_y = Pos_y[frame_ind]
                pos_x_um = Pos_x_um[frame_ind]
                
                #find position of pos_x in dataset, so the correct contour can
                #be assignedset_filtered['pos_x_um']
                inds = [np.nonzero(data_set_filtered['pos_x']==xx)[0]
                        for xx in pos_x_um]
                inds = np.array(inds).flatten()
                
                contours = [data_set_filtered['contour'][i]
                            # for i in ind_unfiltered]
                            for i in inds]
                            
                #write video to see how detection worked out
                if len(frame_ind)>0:
                    for cont in range(len(frame_ind)):
                        #get object index
                        obj_ind = objects[cont]
                        #get contour object in format suited for 
                        #cv2.drawContours
                        #dtype=int necessary for data from tdms files
                        cont_ii = [np.array(contours[cont], dtype=int)]
                        #draw closed colored contour around object
                        cv2.drawContours(image, cont_ii, -1,
                                         colors[obj_ind % len(colors)], -1)
                        #write object index above object
                        cv2.putText(image, str(obj_ind), 
                                    (int(pos_x[cont])-5,int(pos_y[cont])),
                                    font, 0.5, (255,255,255),2,cv2.LINE_AA)
                        
                        if drawZones:
                            #add text to original and overlay, otherway text 
                            #would appear transparent in final image
                            cv2.drawContours(image_out, cont_ii, -1,
                                             colors[obj_ind % len(colors)], -1)
                            #write object index above object
                            cv2.putText(image_out, str(obj_ind), 
                                        (int(pos_x[cont])-5,int(pos_y[cont])),
                                        font, 0.5, (255,255,255),2,cv2.LINE_AA)
    
            if drawZones:
                alpha = 0.3
                cv2.addWeighted(image_out, alpha, image, 1 - alpha, 0, image)
    
            #extend image with black pixels, so each dimension is dividable by
            #macro_block_size; needed for vid codecs to work correctly
            image = extend_img(image, macro_block_size = macro_block_size)
            
    #        try:
    #            vidwriter.append_data(image)
    #        except OSError:
    #            err_txt = ("The video you are trying to overwrite is still opened."
    #                       + " Please close the file and click OK")
    #            ok = None
    #            while ok!="OK":
    #                ok = pymsgbox.alert(text=err_txt, title="Broken Pipe Error")
                
            vidwriter.append_data(image)
            
        except:
            print("Error while video writing. Closing video file.")
            print(traceback.print_exc())
            vidwriter.close()
            break
            
    vidwriter.close()