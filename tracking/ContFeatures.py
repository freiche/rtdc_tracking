# -*- coding: utf-8 -*-
"""
Created on Mon Feb 18 15:55:01 2019

@author: s8570974
"""

import numpy as np
from shapely.geometry import Polygon
from dclab.features import inert_ratio
from scipy.spatial import ConvexHull

def area_polygon(contour):
    
    return Polygon(contour).area
    
def centroid_polygon(contour):
    """Compute the center off mass of a closed contour/polygon using Greens 
    formula.
    https://en.wikipedia.org/wiki/Centroid#Of_a_polygon
    Advantage to using cv2.moments: cv2.moments returns result as integer 
    -> not exact
    
    returns: *tuple* x_com, y_com
    """
    
    centr = np.array(Polygon(contour).centroid.coords)[0]
    
    return centr[0], centr[1]

def deformation(contour):
    
    return 1 - (2*np.sqrt(np.pi * Polygon(contour).area) 
                / Polygon(contour).length)

def convex_hull(contour):
    hull_ind = ConvexHull(contour).vertices
    hull = contour[hull_ind]
    
    return hull
    
def curvature(contour, com_transform=True):
    '''Compute curvature (1/radius) for every point in a contour
    Based on answer to 
    https://stackoverflow.com/questions/28269379/curve-curvature-in-numpy
    '''
    if com_transform:
        com = centroid_polygon(contour)
        contour = contour - np.array(com)
    
    #make contour periodic at borders, np.gradient works with 2nd order
    #difference    
    cont = np.vstack((contour,contour[0],contour[1]))
    cont = np.vstack((contour[-2],contour[-1],cont))

    dx_dt = np.gradient(cont[:, 0])
    dy_dt = np.gradient(cont[:, 1])
    d2x_dt2 = np.gradient(dx_dt)
    d2y_dt2 = np.gradient(dy_dt)
    curvature = (np.abs(d2x_dt2 * dy_dt - dx_dt * d2y_dt2) 
                / (dx_dt**2  + dy_dt**2)**1.5)
    
    #first and last two values dummy values for correct calculation at border
    return curvature[2:-2]

def inertia_ratio(contour):
    '''Correctly compute inertia ratio of a arbitrary contour by shifting it to
    the center of mass. (Missing for dc_lab)'''
    
    com = centroid_polygon(contour)
    cont_com = contour - np.array(com)
    iratio = inert_ratio.get_inert_ratio_raw(cont_com)
    
    return iratio