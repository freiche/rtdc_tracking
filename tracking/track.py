"""
Created on Tue Jul  9 06:46:25 2019

@author: s8570974
"""
import os
import sys
from glob import glob
import numpy as np
from time import time
# from itertools import chain
from scipy.optimize import curve_fit
# import traceback
import cv2
# import dclab
from tqdm import tqdm, trange

from . import flow_velocity as fv
from . import polygon_filter as polyf


def define_rect(image, resize=1, win_name="Draw rectangle", save_path=""):
    """
    Define a rectangular window by click and drag your mouse.

    Parameters
    ----------
    image: Input image.
    """
    global rect_pts, drawing, clone

    drawing = False
    image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)
    image = cv2.resize(image, None, fx=resize, fy=resize,
                       interpolation=cv2.INTER_CUBIC)
    clone = image.copy()
    rect_pts = []  # Starting and ending points

    def select_points(event, x, y, flags, param):

        global rect_pts, drawing, clone
        if event == cv2.EVENT_LBUTTONDOWN and not rect_pts:
            # if (event == cv2.EVENT_LBUTTONDOWN and not rect_pts
            # or len(rect_pts) == 2):
            rect_pts = [(x, y)]
            drawing = True

        elif event == cv2.EVENT_MOUSEMOVE:
            # constantly update rectangle while mouse moves
            if drawing == True:
                clone = image.copy()
                cv2.rectangle(clone, rect_pts[0], (x, y), (0, 0, 255), 1)
                cv2.imshow(win_name, clone)

        elif event == cv2.EVENT_LBUTTONDOWN and len(rect_pts) == 1:
            rect_pts.append((x, y))
            drawing = False

            # draw a rectangle around the region of interest
            cv2.rectangle(clone, rect_pts[0], rect_pts[1], (0, 255, 0), 2)
            cv2.imshow(win_name, clone)

    cv2.namedWindow(win_name)
    cv2.setMouseCallback(win_name, select_points)

    while True:
        # display the image and wait for a keypress
        cv2.imshow(win_name, clone)
        key = cv2.waitKey(0) & 0xFF

        if key == ord("r"):  # Hit 'r' to replot the image
            clone = image.copy()
            rect_pts = []

        elif key == 13:  # Hit <Enter> to confirm the selection
            if rect_pts == []:
                rect_pts = [(0, 0), (len(image[0, :]), len(image[:, 0]))]
            break

    # close the open windows
    cv2.destroyWindow(win_name)

    if save_path:
        np.savetxt(save_path, rect_pts)
    return rect_pts


def define_ROI(image, resize=1):
    win_name = ("Mark region of interest with rectangle. Confirm selection "
                "with Enter. "
                "Redo selection by pressing 'r'. "
                "Press Enter without selecting anything to use whole image"
                )  # Window name

    return define_rect(image, resize=resize, win_name=win_name)


def get_ROI(image, resize=1.):
    '''Return image cropped for region set in define_ROI'''
    # select region of interest from channel image
    win_name = ("Mark region of interest with rectangle. Confirm selection "
                "with Enter. "
                "Redo selection by pressing 'r'. "
                "Press Enter without selecting anything to use whole image"
                )  # Window name

    roi_pts = define_rect(image, resize=resize, win_name=win_name)
    pos_x_min = min([roi_pts[0][0] / resize, roi_pts[1][0] / resize])
    pos_x_max = max([roi_pts[0][0] / resize, roi_pts[1][0] / resize])
    pos_y_min = min([roi_pts[0][1] / resize, roi_pts[1][1] / resize])
    pos_y_max = max([roi_pts[0][1] / resize, roi_pts[1][1] / resize])

    img_roi = image[int(pos_y_min):int(pos_y_max),
              int(pos_x_min):int(pos_x_max)]

    boundaries_dict = {'pos_x_min': pos_x_min, 'pos_x_max': pos_x_max,
                       'pos_y_min': pos_y_min, 'pos_y_max': pos_y_max}

    return img_roi, boundaries_dict


def get_bgimage(sample_path, measurement_no):
    # check if .rtdc files exist:
    if glob(os.path.join(sample_path, '*rtdc')):
        mno_string = "M" + str(1000 + measurement_no)[1:]
        imfile = sample_path + "\\" + mno_string + "_bg.png"
        imfile = os.path.join(sample_path, mno_string + "_bg.png")
    elif glob(os.path.join(sample_path, '*tdms')):
        mno_string = "M" + str(measurement_no)
        imfile = os.path.join(sample_path, mno_string + "_bg_102.png")
    else:
        sys.exit("No data in suitable format found")

    img = cv2.imread(imfile)
    img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

    return img


def define_channel(image, resize=1):
    win_name = ("Mark channel with rectangle. Confirm selection with Enter. "
                "Redo selection by pressing 'r'. "
                "Press Enter without selecting anything to use whole image"
                )  # Window name

    return define_rect(image, resize=resize, win_name=win_name)


def get_channel_pts(measurement_file, img_resize=1., channelpath=None):
    if not channelpath:
        channelpath = find_analysis_path(measurement_file) + "channelpts.txt"

    if os.path.isfile(channelpath):
        # print(channelpath)
        rect_pts = np.loadtxt(channelpath)
    else:
        if measurement_file.endswith('.tdms'):
            imfile = measurement_file.strip("data.tdms") + "bg_102.png"
        elif measurement_file.endswith('.rtdc'):
            imfile = measurement_file.strip("data.rtdc") + "bg.png"

        img = cv2.imread(imfile)
        img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        rect_pts = define_rect(img, resize=img_resize,
                               win_name="Select channel region",
                               save_path=channelpath)

    detection_zone = [rect_pts[0][0] / img_resize, rect_pts[1][0] / img_resize]
    detection_zone.sort()

    return detection_zone


### Object tracking ###
def med_displacement(frames, pos_x):
    """
    Median displacement of cells in consecutive frames moving in the same
    direction. Used to predict dislacement of the objects.
    
    Parameters:
        frames: *list* or *ndarray*
            Frame information from the experiment
        pos_x: *list* or *ndarray*
            X-positions from the experiment
            
    Returns:
        out: *tuple: (float, float)*
            med_x_displacement: median displacement in x
            Delta_x_displacement: 2*std(displacement) 
    """

    frameNrsUniq, uniqIndex = np.unique(frames, return_index=True)
    # get average velocity from single cell events only
    pos_xUniq = np.array(pos_x)[uniqIndex]

    pos_x_diff = np.diff(pos_xUniq) * -1  # object moving forward results in neg value
    # throw out all negative values --> here a new object appeared
    # only take displacement of consecutive events
    consecFrames = np.diff(frameNrsUniq)[pos_x_diff > 0]
    pos_x_diff = pos_x_diff[pos_x_diff > 0]
    pos_x_diff = pos_x_diff[consecFrames == 1]
    med_x_displacement = np.median(pos_x_diff)

    # cut off outliers in data for more reliable value
    # data = data within 1 sigma intervall
    pos_x_diff = pos_x_diff[pos_x_diff > med_x_displacement - np.std(pos_x_diff)]
    pos_x_diff = pos_x_diff[pos_x_diff < med_x_displacement + np.std(pos_x_diff)]
    med_x_displacement = np.median(pos_x_diff)

    # Uncertainty for the median displacement of the objects
    Delta_x_displacement = 2 * np.std(pos_x_diff)

    return med_x_displacement, Delta_x_displacement


def seperate_objects(DATA, WindowSize=25):
    """
    OBSOLETE
    
    Assign object number to the events detecetd with GetVidData.
    
    Parameters:
        DATA: *dictionary*
            DATA dictionary from GetVidData
        WindowSize: *float*
            Minimum size of the tolerance region in um for predicting where 
            object will be in the next frame.
            
    Returns:
        out: *dictionary*
            Dictionary that contains info sorted for each object. keys are of 
            format 'Object 0', 'Object 1', ...
    """
    # pos_x[i] + (med_x_displacement +/- Delta_x_displacement) creates a zone where
    # the obect from frame i is expected in frame i+1
    frames = np.array(DATA['frame'])
    Index = np.array(DATA['index'])
    Pos_x = np.array(DATA['pos_x'])
    frames_uniq = np.unique(frames)
    X_positions = []
    EventIndices = []

    med_x_displacement, Delta_x_displacement = med_displacement(frames, Pos_x)

    if Delta_x_displacement < WindowSize:
        Delta_x_displacement = WindowSize

    # create list that contains all entries in each frame for pos_x and event_index
    for frame in frames_uniq:
        ind = np.nonzero(frames == frame)[0]
        X_positions.append(Pos_x[ind])
        EventIndices.append(Index[ind])

    # dictionary to save object Data
    Objects = {}

    # create list that contains objects that should be kept track off
    tracked_objects = [0]

    print("Object assignment")
    print("Iteration over frames")
    for frame_ind in trange(len(frames_uniq)):

        if frame_ind == 0:
            stop_tracking = []
        else:
            for remove in stop_tracking:
                tracked_objects.remove(remove)
            stop_tracking = []

        frame = frames_uniq[frame_ind]
        # Iterate over events in the frame
        for i in range(len(EventIndices[frame_ind])):
            Ind = EventIndices[frame_ind][i]
            X = X_positions[frame_ind][i]

            if frame_ind == 0:
                key = "Object 0"
                Objects[key] = [(frame, Ind, X)]
            else:
                Found = False
                for tr_obj in tracked_objects:
                    key = "Object " + str(tr_obj)
                    prev_X = Objects[key][-1][-1]
                    prev_frame = Objects[key][-1][0]

                    # recalculate median displacement for the object
                    if len(Objects[key]) > 3:
                        # get object path so far
                        obj_X = np.array(Objects[key])[:, 2]
                        obj_frames = np.array(Objects[key])[:, 0]

                        # two events in same frame might be assigned to same
                        # object. Gets filtered out later
                        if len(obj_frames) == len(np.unique(obj_frames)):
                            displacement = (np.diff(obj_X)
                                            / np.diff(obj_frames))
                            # For med_displacement don't take into account if
                            # cell got stuck at a constriction: displacement
                            # must be larger than 1!
                            displacement = displacement[displacement > 1]
                            # recalculate median displacement
                            if len(displacement) > 5:
                                med_x_displacement = np.median(displacement)

                    predZoneStart = (prev_X - (frame - prev_frame)
                                     * med_x_displacement
                                     - Delta_x_displacement)
                    predZoneEnd = (prev_X - (frame - prev_frame)
                                   * med_x_displacement
                                   + Delta_x_displacement)
                    prediction_zone = (predZoneStart, predZoneEnd)

                    # check if prediction zone is outside the channel
                    if prediction_zone[1] < 0:
                        stop_tracking.append(tr_obj)
                    else:
                        if X > prediction_zone[0] and X < prediction_zone[1]:
                            Objects[key].append((frame, Ind, X))
                            Found = True

                if Found == False:
                    new_obj_nr = len(Objects)
                    key = "Object " + str(new_obj_nr)
                    tracked_objects.append(new_obj_nr)
                    Objects[key] = [(frame, Ind, X)]

                # list might contain same element twice --> removing procedure
                # causes error --> get rid of duplicates
                stop_tracking = list(np.unique(stop_tracking))

    print("\n")
    return Objects


def seperate_objects_new(DATA, flowrate, channel_size, channel_zone,
                         frame_rate, WindowSize=25):
    """
    OBSOLETE
    
    Assign object number to the events detecetd with GetVidData.
    
    Parameters:
        DATA: *dictionary*
            DATA dictionary from GetVidData
        WindowSize: *float*
            Minimum size of the tolerance region in um for predicting where 
            object will be in the next frame.
            
    Returns:
        out: *dictionary*
            Dictionary that contains info sorted for each object. keys are of 
            format 'Object 0', 'Object 1', ...
    """
    # pos_x[i] + (med_x_displacement +/- Delta_x_displacement) creates a zone where
    # the obect from frame i is expected in frame i+1
    frames = np.array(DATA['frame'])
    Index = np.array(DATA['index'])
    Pos_x = np.array(DATA['pos_x']) * 0.34  # convert back to um
    size_y = np.array(DATA['size_y'])

    frames_uniq = np.unique(frames)
    X_positions = []
    EventIndices = []
    Sizes_y = []

    #    med_x_displacement, Delta_x_displacement = med_displacement(frames, Pos_x)

    #    if Delta_x_displacement < WindowSize:
    #        Delta_x_displacement = WindowSize

    # create list that contains all entries in each frame for pos_x and event_index
    for frame in frames_uniq:
        ind = np.nonzero(frames == frame)[0]
        X_positions.append(Pos_x[ind])
        EventIndices.append(Index[ind])
        Sizes_y.append(size_y[ind])

    # dictionary to save object Data
    Objects = {}

    # create list that contains objects that should be kept track of
    tracked_objects = [0]

    for frame_ind in range(len(frames_uniq)):

        if frame_ind == 0:
            stop_tracking = []
        else:
            for remove in stop_tracking:
                tracked_objects.remove(remove)
            stop_tracking = []

        frame = frames_uniq[frame_ind]
        # Iterate over events in the frame
        for i in range(len(EventIndices[frame_ind])):
            Ind = EventIndices[frame_ind][i]
            X = X_positions[frame_ind][i]
            y_size = Sizes_y[frame_ind][i]

            if frame_ind == 0:
                key = "Object 0"
                Objects[key] = [(frame, Ind, X, y_size)]
            else:
                Found = False
                for tr_obj in tracked_objects:
                    key = "Object " + str(tr_obj)
                    prev_X = Objects[key][-1][2]
                    prev_frame = Objects[key][-1][0]
                    prev_ysize = Objects[key][-1][3]
                    # calculate velocity to predict displacement
                    #                    v_prevframe = flow_velocity.VelocityAdvectedSphere(
                    #                            flowrate, channel_size, prev_ysize,
                    #                            channelType='square')[0]

                    v_prevframe = fv.velocity_predict(flowrate, prev_X,
                                                      channel_size, prev_ysize / 2.,
                                                      channel_zone)
                    # displacement in um
                    displacement = (v_prevframe * 1e6 * (frame - prev_frame)
                                    / frame_rate)

                    predZoneStart = prev_X - displacement - WindowSize / 2.
                    predZoneEnd = prev_X - displacement + WindowSize / 2.
                    prediction_zone = (predZoneStart, predZoneEnd)

                    # check if prediction zone is outside the channel
                    if prediction_zone[1] < 0:
                        stop_tracking.append(tr_obj)
                    else:
                        if prediction_zone[0] < X < prediction_zone[1]:
                            Objects[key].append((frame, Ind, X, y_size))
                            Found = True

                if Found == False:
                    new_obj_nr = len(Objects)
                    key = "Object " + str(new_obj_nr)
                    tracked_objects.append(new_obj_nr)
                    Objects[key] = [(frame, Ind, X, y_size)]

                # list might contain same element twice --> removing procedure
                # causes error --> get rid of duplicates
                stop_tracking = list(np.unique(stop_tracking))

    return Objects


def seperate_objects_dclab(dataset, WindowSize=25):
    """
    OBSOLETE
    TODO: Adapt changes in seperate_objects_dev here so it can be used natively by Paul on .rtdc files
    
    Assign object number to the events detecetd with GetVidData.
    
    Parameters:
        DATA: *dictionary*
            DATA dictionary from GetVidData
        WindowSize: *float*
            Minimum size of the tolerance region in um for predicting where 
            object will be in the next frame.
            
    Returns:
        out: *dictionary*
            Dictionary that contains info sorted for each object. keys are of 
            format 'Object 0', 'Object 1', ...
    """
    # pos_x[i] + (med_x_displacement +/- Delta_x_displacement) creates a zone where
    # the obect from frame i is expected in frame i+1
    frames = np.array(dataset['frame'])
    Index = np.array(dataset['index']) - 1  # dclab index starts at 1
    Pos_x = np.array(dataset['pos_x'])
    frames_uniq = np.unique(frames)
    X_positions = []
    EventIndices = []

    med_x_displacement, Delta_x_displacement = med_displacement(frames, Pos_x)

    if Delta_x_displacement < WindowSize:
        Delta_x_displacement = WindowSize

    # create list that contains all entries in each frame for pos_x and event_index
    for frame in frames_uniq:
        ind = np.nonzero(frames == frame)[0]
        X_positions.append(Pos_x[ind])
        EventIndices.append(Index[ind])

    # dictionary to save object Data
    Objects = {}

    # create list that contains objects that should be kept track off
    tracked_objects = [0]

    print("Object assignment")
    print("Iteration over frames")
    for frame_ind in trange(len(frames_uniq)):

        if frame_ind == 0:
            stop_tracking = []
        else:
            for remove in stop_tracking:
                tracked_objects.remove(remove)
            stop_tracking = []

        frame = frames_uniq[frame_ind]
        # Iterate over events in the frame
        for i in range(len(EventIndices[frame_ind])):
            Ind = EventIndices[frame_ind][i]
            X = X_positions[frame_ind][i]

            if frame_ind == 0:
                key = "Object 0"
                Objects[key] = [(frame, Ind, X)]
            else:
                Found = False
                for tr_obj in tracked_objects:
                    key = "Object " + str(tr_obj)
                    prev_X = Objects[key][-1][-1]
                    prev_frame = Objects[key][-1][0]

                    # recalculate median displacement for the object
                    if len(Objects[key]) > 3:
                        # get object path so far
                        obj_X = np.array(Objects[key])[:, 2]
                        obj_frames = np.array(Objects[key])[:, 0]

                        # two events in same frame might be assigned to same
                        # object. Gets filtered out later
                        if len(obj_frames) == len(np.unique(obj_frames)):
                            displacement = (np.diff(obj_X)
                                            / np.diff(obj_frames))
                            # For med_displacement don't take into account if
                            # cell got stuck at a constriction: displacement
                            # must be larger than 1!
                            displacement = displacement[displacement > 1]
                            # recalculate median displacement
                            if len(displacement) > 5:
                                med_x_displacement = np.median(displacement)

                    predZoneStart = (prev_X - (frame - prev_frame)
                                     * med_x_displacement
                                     - Delta_x_displacement)
                    predZoneEnd = (prev_X - (frame - prev_frame)
                                   * med_x_displacement
                                   + Delta_x_displacement)
                    prediction_zone = (predZoneStart, predZoneEnd)

                    # check if prediction zone is outside the channel
                    if prediction_zone[1] < 0:
                        stop_tracking.append(tr_obj)
                    else:
                        if prediction_zone[0] < X < prediction_zone[1]:
                            Objects[key].append((frame, Ind, X))
                            Found = True

                if Found == False:
                    new_obj_nr = len(Objects)
                    key = "Object " + str(new_obj_nr)
                    tracked_objects.append(new_obj_nr)
                    Objects[key] = [(frame, Ind, X)]

                # list might contain same element twice --> removing procedure
                # causes error --> get rid of duplicates
                stop_tracking = list(np.unique(stop_tracking))

    print("\n")
    return Objects


def get_ObjectData(Objects, DATA):
    """Assign parameters to the Objects dictionary determined before.

    Parameters:
        Objects: *dictionary*
            Objects dictionary from fct seperate_objects
        DATA: *dictionary*
            DATA dictionary with video data from GetVidData
    Returns:
        out: *dictionary*, *dictionary*
            DATA, ObjectsData\n
            DATA: dict like DATA with assigned parameters for every event
            ObjectsDATA: dict where data is sorted for each object, keys are of 
            format 'Object 0', 'Object 1', ...
    """
    # Create new dictionary where parameters are assigned for every object and
    # frame
    ObjectsData = {}

    object_index = np.zeros_like(DATA['frame'])

    print("Assign Data to objects")
    print("Iteration over found objects")
    for j in trange(len(Objects)):
        key = 'Object ' + str(j)
        objectdata = {}
        ind = np.array(Objects[key], dtype=int)[:, 1]

        for DataKey in DATA:
            # objectdata[DataKey] = np.array(DATA[DataKey])[ind]
            if (DataKey == 'image' or DataKey == 'contour_raw'):
                objectdata[DataKey] = [DATA[DataKey][ii] for ii in ind]
            else:
                objectdata[DataKey] = DATA[DataKey][ind]

        ObjectsData[key] = objectdata
        object_index[ind] = j

    DATA['object index'] = object_index

    print("\n")
    return DATA, ObjectsData


def get_ObjectData_scalarOnly(Objects, DATA):
    """Assign only scalar parameters to the Objects dictionary determined 
    before. Don't assign any non-scalar parameters like images or contours. Can
    be used to decrease memory usage.

    Parameters:
        Objects: *dictionary*
            Objects dictionary from fct seperate_objects
        DATA: *dictionary*
            DATA dictionary with video data from GetVidData
    Returns:
        out: *dictionary*, *dictionary*
            DATA, ObjectsData\n
            DATA: dict like DATA with assigned parameters for every event
            ObjectsDATA: dict where data is sorted for each object, keys are of 
            format 'Object 0', 'Object 1', ...
    """
    # Create new dictionary where parameters are assigned for every object and
    # frame
    ObjectsData = {}

    object_index = np.zeros_like(DATA['frame'])

    print("Assign Data to objects")
    print("Iteration over found objects")
    for j in trange(len(Objects)):
        key = 'Object ' + str(j)
        objectdata = {}
        ind = np.array(Objects[key], dtype=int)[:, 1]

        for DataKey in DATA:
            #            objectdata[DataKey] = np.array(DATA[DataKey])[ind]
            if (DataKey == 'image' or DataKey == 'contour_raw'):
                pass
            else:
                objectdata[DataKey] = DATA[DataKey][ind]

        ObjectsData[key] = objectdata
        object_index[ind] = j

    DATA['object index'] = object_index

    print("\n")
    return DATA, ObjectsData


def get_ObjectData_dclab(Objects, DATA, dataset):
    """
    THIS IS SLOWER THAN WORKING WITH THE CLASSIC DICT (get_ObjectData)
    
    Assign parameters to the Objects dictionary determined before.

    Parameters:
        Objects: *dictionary*
            Objects dictionary from fct seperate_objects
        DATA: *dictionary*
            DATA dictionary with video data from GetVidData
    Returns:
        out: *dictionary*, *dictionary*
            DATA, ObjectsData\n
            DATA: dict like DATA with assigned parameters for every event
            ObjectsDATA: dict where data is sorted for each object, keys are of 
            format 'Object 0', 'Object 1', ...
    """
    # Create new dictionary where parameters are assigned for every object and
    # frame
    ObjectsData = {}

    object_index = np.zeros_like(dataset['frame'])

    print("Assign Data to objects")
    print("Iteration over found objects")
    for j in trange(len(Objects)):
        key = 'Object ' + str(j)
        objectdata = {}
        ind = np.array(Objects[key], dtype=int)[:, 1]

        # for DataKey in DATA:
        #   objectdata[DataKey] = np.array(DATA[DataKey])[ind]
        for feat in dataset.features:
            if (feat == 'image'  # or feat == 'contour'
                    or feat == 'mask'):  # or feat == 'trace'):
                objectdata[feat] = [dataset[feat][ii] for ii in ind]
            elif feat == 'contour' or feat == 'trace':
                pass
            else:
                objectdata[feat] = dataset[feat][ind]

        ObjectsData[key] = objectdata
        object_index[ind] = j

    DATA['object index'] = object_index

    print("\n")
    return DATA, ObjectsData


def get_ObjectIndex(Objects, DATA):
    """Assign only the index from the dclab dataset to the Objects dictionary 
    determined before. Might be useful to make script more memory efficient.

    Parameters:
        Objects: *dictionary*
            Objects dictionary from fct seperate_objects
        DATA: *dictionary*
            DATA dictionary with video data from GetVidData
    Returns:
        out: *dictionary*, *dictionary*
            DATA, ObjectsData\n
            DATA: dict like DATA with assigned parameters for every event
            ObjectsIndex: dict where indices are sorted for each object, keys 
            are of format 'Object 0', 'Object 1', ...
    """
    # Create new dictionary where parameters are assigned for every object and
    # frame
    ObjectsIndex = {}

    object_index = np.zeros_like(DATA['frame'])

    print("Assign dataset indices to objects")
    print("Iteration over found objects")
    for j in trange(len(Objects)):
        key = 'Object ' + str(j)
        ind = np.array(Objects[key], dtype=int)[:, 1]
        ObjectsIndex[key] = DATA['index'][ind]
        object_index[ind] = j

    DATA['object index'] = object_index

    print("\n")
    return DATA, ObjectsIndex


def filter_ObjectsDATA(ObjectsData_dict, DATA, min_events=3,
                       **kwargs):
    """Filter the ObjectsData dictionary for unwanted events: 2 objects in same
    prediction zone; Object path not from beginning to end. Filtered objects 
    will be deleted from ObjectsData_dict.
    
    Parameters: 
        ObjectsData_dict: *dictionary*
            Dictionary containing objects data acquired with get_ObjectData().
            Filtered objects will be deleted from this dictionary.
        DATA: *dictionary*
            DATA dictionary with video data from GetVidData
        min_events: *int*
            Minimum number of events per object to be taken into account 

    out: *dictionary*, *dictionary*
        DATA, ObjectsData\n
        DATA_filtered
            DATA dictionary filtered for unwanted events
        ObjectsDATA
            dict where data is sorted for each object, keys are of format
            'Object 0', 'Object 1', ...
    """
    # filter the Objectsdata dict for bad events
    # save the keys of the bad objects in list and iterate over them later and
    # delete
    del_keys = []
    DATA_filtered = DATA.copy()

    print("Filter objects")
    # Filter two cells if distance between them is shorter than min_obj_distance
    # in um
    if 'min_obj_distance' in kwargs:
        min_dist = kwargs['min_obj_distance']
        min_obj_distance_keys = []

        for frame in np.unique(DATA['frame']):
            frame_ind = np.nonzero(DATA['frame'] == frame)[0]
            pos_x_frame = np.sort(DATA['pos_x_um'][frame_ind])
            distances = np.diff(pos_x_frame)
            # find where distances are smaller than min_obj_distance
            ind_tooshort = np.nonzero(np.abs(distances) < min_dist)[0]
            # exclude both leading and trailing object
            ind_tooshort_plus1 = ind_tooshort + 1
            ind_tooshort = np.append(ind_tooshort, ind_tooshort_plus1)
            # find correct objectnr in DATA dict and append to del_keys
            del_obj_nr = DATA['object index'][frame_ind][ind_tooshort]
            for ii in del_obj_nr:
                del_keys.append("Object " + str(int(ii)))
                min_obj_distance_keys.append(int(ii))

    print("Iterating over Objects")
    err_data_writing = []
    same_pred_zone = []
    not_min_events = []
    for key in tqdm(ObjectsData_dict.keys()):
        obj_nr = int(key.split(" ")[-1])
        obj_ind = np.nonzero(DATA_filtered['object index'] == obj_nr)[0]

        # if more events where written to ObjectsData then available in DATA,
        # something went wrong while data writing
        if (len(np.array(DATA['frame'])[obj_ind])
                != len(ObjectsData_dict[key]['frame'])):

            del_keys.append(key)
            err_data_writing.append(key)

        # exclude two ore more objects in same pred_zone
        elif (len(np.unique(ObjectsData_dict[key]['frame']))
              != len(ObjectsData_dict[key]['frame'])):

            del_keys.append(key)
            same_pred_zone.append(key)

        # minimum number of events required per cell
        elif len(ObjectsData_dict[key]['frame']) < min_events:
            del_keys.append(key)
            not_min_events.append(key)

        # measured cells must make it from channel entrance to exit
        # make this optional with the detection zone variable in kwargs
        elif 'detection_zone' in kwargs:
            dzone = kwargs['detection_zone']

            # set the point (in pixel) before/after the channel the object has
            # to pass to be taken into account for the measurement
            # max_ch_offset=0 means the object must be detected before and after
            # the channel to be taken
            if 'max_ch_offset' in kwargs:
                max_ch_offset = kwargs['max_ch_offset']
            else:
                max_ch_offset = 0

            if (ObjectsData_dict[key]['pos_x'][0] < dzone[1] + max_ch_offset
                    or ObjectsData_dict[key]['pos_x'][-1] > dzone[0] - max_ch_offset):
                del_keys.append(key)

    print("Filtered objects: {0}".format(len(set(del_keys))))

    for key in set(del_keys):
        ObjectsData_dict.pop(key)

        obj_nr = int(key.split(" ")[-1])
        obj_ind = np.nonzero(DATA_filtered['object index'] == obj_nr)[0]

        ###############################################################################
        ###### out commented because causes memory error for large datasets ###########
        ###############################################################################
        for datakey in DATA.keys():
            DATA_filtered[datakey] = np.delete(np.array(DATA_filtered[datakey]),
                                               obj_ind)
    ###############################################################################

    print("\n")
    print("filtered bc of min_obj_distance:", len(min_obj_distance_keys))
    print("filtered bc of error during data writing:", len(err_data_writing))
    print("filtered bc of same pred zone:", len(same_pred_zone))
    print("filtered bc of not min events:", len(not_min_events))
    return DATA_filtered, ObjectsData_dict


def get_VelocityAndTime(ObjectsData, DATA, channel_zone, FPS, PIX_Size):
    """Add velocity and time information to the ObjectsData dictionary and DATA
    dict. For every object velocity[0] = velocity[1] and time[0] is determined 
    by velocity[0] and the distance of the object to channel entry. 
    
    Parameters:
        ObjectsData: *dictionary*
            Dictionary containing objects data acquired with get_ObjectData().
            Filtered objects will be deleted from this dictionary.
        DATA: *dictionary*
            DATA dictionary with video data from GetVidData
        channel_zone: *list*
            Coordinates of the channel borders in pixels
        FPS: *int*
            Frame of the experiment
        PIX_size: *float*
            Pixel size of the experiment
            
    out: *dictionary*, *dictionary*
        DATA, ObjectsData\n
        DATA_filtered
            DATA dictionary filtered for unwanted events
        ObjectsDATA
            dict where data is sorted for each object, keys are of format
            'Object 0', 'Object 1', ...
    """
    DATA_index = DATA['index']
    DATA['velocity'] = np.array([np.nan] * len(DATA_index))
    DATA['time'] = np.array([np.nan] * len(DATA_index))
    DATA['t_outlet'] = np.array([np.nan] * len(DATA_index))
    DATA['velocity_frame0'] = np.array([np.nan] * len(DATA_index))
    DATA['time_frame0'] = np.array([np.nan] * len(DATA_index))
    DATA['t_outlet_frame0'] = np.array([np.nan] * len(DATA_index))
    # velocity values gets assigned to middle position between two frames
    # DATA['pos_x_frame_middle'] = np.array([np.nan]*len(DATA_index))

    if 'frame_tdms' in DATA.keys():
        tdms_flag = True
    else:
        tdms_flag = False

    print("Assigning Velocity and time")
    print("Iteration over Objects")
    for key in tqdm(ObjectsData.keys()):
        try:
            pos_x = ObjectsData[key]['pos_x']
            # only objects in channel
            pos_x_channel = pos_x[(pos_x < channel_zone[1])
                                  & (pos_x > channel_zone[0])]
            # objects tracked after leaving the channel
            pos_x_outlet = pos_x[pos_x < channel_zone[0]]
            # find the middle position between two frame
            # x_middle = (x[i+1]-x[i])/2.+x[i]
            # pos_x_frame_middle = np.diff(pos_x)/2. + pos_x[:-1]

            # improve: find solution if object was not tracked inside channel at all
            if len(pos_x_channel) > 0:
                # find index when object is first time in channel
                # =largest value of pos_x
                ind_inChannel = np.nonzero(pos_x == max(pos_x_channel))[0]
                # ind_inChannel = np.nonzero(pos_x==pos_x_channel[0])[0]
                in_channel = True
            else:
                # ind_inChannel = 0
                in_channel = False

            if len(pos_x_outlet) > 0:
                # find index when object is first time left channel
                # =largest value of pos_x_outlet
                ind_outlet = np.nonzero(pos_x == max(pos_x_outlet))[0]
                in_outlet = True
            else:
                in_outlet = False

            if tdms_flag:
                frames = ObjectsData[key]['frame_tdms']
            else:
                frames = ObjectsData[key]['frame']

            velocity = np.array([np.nan] * len(frames))
            time = np.array([np.nan] * len(frames))
            time_outlet = np.array([np.nan] * len(frames))
            # assign the velocity value to first frame, not second
            velocity_frame0 = np.array([np.nan] * len(frames))
            time_frame0 = np.array([np.nan] * len(frames))
            time_outlet_frame0 = np.array([np.nan] * len(frames))

            Delta_frames = np.diff(frames)
            if len(Delta_frames) > 1:

                # velocity in um/s
                velocity[1:] = abs(np.diff(pos_x)) * PIX_Size / Delta_frames * FPS
                velocity_frame0[:-1] = (abs(np.diff(pos_x))
                                        * PIX_Size / Delta_frames * FPS)
                if in_channel:
                    if 0 in ind_inChannel:
                        velocity[0] = velocity[1]
                    else:
                        velocity[0] = np.nan
                    velocity_frame0[-1] = velocity_frame0[-2]

                    time0 = ((channel_zone[1] - pos_x[ind_inChannel]) * PIX_Size
                             / velocity[ind_inChannel])
                    time = (frames - frames[ind_inChannel]) / float(FPS) + time0

                    # If computed with velocity_frame0
                    time0 = ((channel_zone[1] - pos_x[ind_inChannel]) * PIX_Size
                             / velocity_frame0[ind_inChannel])
                    time_frame0 = ((frames - frames[ind_inChannel])
                                   / float(FPS) + time0)

                if in_outlet:
                    time0 = ((channel_zone[0] - pos_x[ind_outlet]) * PIX_Size
                             / velocity[ind_outlet])
                    time_outlet = (frames - frames[ind_outlet]) / float(FPS) + time0

                    # If computed with velocity_frame0
                    time0 = ((channel_zone[0] - pos_x[ind_outlet]) * PIX_Size
                             / velocity_frame0[ind_outlet])
                    time_outlet_frame0 = (frames - frames[ind_outlet]) / float(FPS) + time0

                # velocity in mm/s
                ObjectsData[key]['velocity'] = velocity / 1000.
                ObjectsData[key]['time'] = time
                ObjectsData[key]['time_outlet'] = time_outlet

                # add DATA to DATA dict
                index = ObjectsData[key]['index']
                for ii in range(len(index)):
                    ind = np.nonzero(DATA_index == index[ii])[0]
                    DATA['velocity'][ind] = velocity[ii] / 1000.
                    DATA['time'][ind] = time[ii]
                    DATA['t_outlet'][ind] = time_outlet[ii]
                    DATA['velocity_frame0'][ind] = velocity_frame0[ii] / 1000.
                    DATA['time_frame0'][ind] = time_frame0[ii]
                    DATA['t_outlet_frame0'][ind] = time_outlet_frame0[ii]
        except:
            continue
    print("\n")
    return DATA, ObjectsData


def get_VelocityAndTime_fromIndex(ObjectsData, DATA, channel_zone, FPS, PIX_Size):
    """Add velocity and time information to the ObjectsData dictionary and DATA
    dict. For every object velocity[0] = velocity[1] and time[0] is determined 
    by velocity[0] and the distance of the object to channel entry. 
    
    Parameters:
        ObjectsData: *dictionary*
            Dictionary containing objects data acquired with get_ObjectData().
            Filtered objects will be deleted from this dictionary.
        DATA: *dictionary*
            DATA dictionary with video data from GetVidData
        channel_zone: *list*
            Coordinates of the channel borders
        FPS: *int*
            Frame of the experiment
        PIX_size: *float*
            Pixel size of the experiment
            
    out: *dictionary*, *dictionary*
        DATA, ObjectsData\n
        DATA_filtered
            DATA dictionary filtered for unwanted events
        ObjectsDATA
            dict where data is sorted for each object, keys are of format
            'Object 0', 'Object 1', ...
    """
    DATA_index = DATA['index']
    DATA['velocity'] = np.zeros(len(DATA_index))
    DATA['time'] = np.zeros(len(DATA_index))

    if 'frame_tdms' in DATA.keys():
        tdms_flag = True
    else:
        tdms_flag = False

    print("Assigning Velocity and time")
    print("Iteration over Objects")
    for key in tqdm(ObjectsData.keys()):

        pos_x = ObjectsData[key]['pos_x']

        if tdms_flag:
            frames = ObjectsData[key]['frame_tdms']
        else:
            frames = ObjectsData[key]['frame']

        velocity = np.zeros_like(frames)
        time = np.zeros_like(frames)

        Delta_frames = np.diff(frames)
        if len(Delta_frames) > 1:

            # velocity in um/s
            velocity[1:] = abs(np.diff(pos_x)) * PIX_Size / Delta_frames * FPS
            velocity[0] = velocity[1]

            time0 = (channel_zone[1] - pos_x[0]) * PIX_Size / velocity[0]
            time = (frames - frames[0]) / float(FPS) + time0

            # velocity in mm/s
            ObjectsData[key]['velocity'] = velocity / 1000.
            ObjectsData[key]['time'] = time

            # add DATA to DATA dict
            index = ObjectsData[key]['index']
            for ii in range(len(index)):
                ind = np.nonzero(DATA_index == index[ii])[0]
                DATA['velocity'][ind] = velocity[ii] / 1000.
                DATA['time'][ind] = time[ii]

    print("\n")
    return DATA, ObjectsData


def sort_DATA_by_objNr(DATA):
    """Sort the data in the DATA dictionary so that it is arranged by 
    objects nr instead of event index.

    Parameters:
        DATA: *dictionary*
            DATA dictionary with video data from GetVidData
    
    out: *dictionary*
        DATA
            DATA dictionary sorted by object number    
    """
    time1 = time()

    obj_nrs = DATA['object index']
    index = np.arange(len(obj_nrs))

    # get unique list of all object indices
    obj_nrs_uniq = np.sort(list(set(obj_nrs)))
    index_new = np.array([])

    for ii in obj_nrs_uniq:
        ind_obj = np.nonzero(obj_nrs == ii)[0]
        # find the indices assigned to the object nr and sort
        index_obj = np.sort(index[ind_obj])
        index_new = np.append(index_new, index_obj)

    ind = np.array(index_new, dtype=int)

    # use new indexing on all data in DATA
    returnDATA = {}
    for key in DATA.keys():
        returnDATA[key] = np.array(DATA[key])[ind]

    print("time for sorting: {:2.2f} s".format(time() - time1))
    print("\n")

    return returnDATA


def DataToTSV(Dict, tsv_path):
    """Write the Data from the DATA dictionary to a .tsv file. Does not include
    contour data!
    
    Parameters:     
        Dict: *dictionary*
            Dictionary that contains the data arrays determined by 
            get_ObjectData
        tsv_path: *string*
            File path where results file will be saved.
    """
    time1 = time()

    header = "\t".join([key for key in Dict.keys()
                        if not (key == 'DFT_coeffs' or key == 'contour_raw'
                                or key == 'conv_hull' or key == 'image')])

    data = [np.array(Dict[key]) for key in Dict.keys()
            if not (key == 'DFT_coeffs' or key == 'contour_raw'
                    or key == 'conv_hull'
                    or key == 'image')]

    if 'DFT_coeffs' in Dict.keys():
        # save real and imaginary part separated by column for each harmonic
        for ind, c in zip(range(len(Dict['DFT_coeffs'])), Dict['DFT_coeffs']):
            data.append(c.real)
            header += "\tDFT_coeff_real_" + str(ind)
            data.append(c.imag)
            header += "\tDFT_coeff_imag_" + str(ind)

    data = np.array(data).transpose()

    with open(tsv_path, "w") as tfile:
        # write header
        tfile.write("# " + header + "\n")
        np.savetxt(tfile, data, delimiter="\t", fmt='%.8f')

    print("time for saving: {:2.2f} s".format(time() - time1))


#########################################
### Functions still under development ###
#########################################
def lin_growth(x, m, n):
    return m * x + n


def RichardsCurve(x, A, K, Q, B, v):
    return A + (K - A) / ((1. + Q * np.exp(-B * x)) ** (1 / v))


def prep_velocityFit(dataset, first_frame=0, last_frame=1000):
    """Pre filter velocity and x-data for before fit of Richard's curve"""
    # find frames with only one event (unique frames)
    # find objects that moved left in consecutive frames
    FPS = dataset.config['imaging']['frame rate']

    size_ds = len(dataset['frame'])

    if last_frame == 0 or last_frame > size_ds:
        last_frame = size_ds

    frames_uniq, index_uniq, counts = np.unique(dataset['frame'][first_frame:last_frame],
                                                return_index=True,
                                                return_counts=True)
    # frames_uniq still contains the frame number where two events occured
    # only take frames that were counted only once
    frames_uniq = frames_uniq[np.nonzero(counts == 1)[0]]
    index_uniq = index_uniq[np.nonzero(counts == 1)[0]]

    # only consecutive frames
    index_uniq = index_uniq[1:][np.diff(frames_uniq) == 1]
    frames_uniq = frames_uniq[1:][np.diff(frames_uniq) == 1]

    # add offset from first frame to index_uniq
    index_uniq += first_frame

    pos_x_uniq = dataset['pos_x'][index_uniq]

    # flow from right to left causes negative velocity values
    # velocity in um/s
    # convert to positive values because works better
    # for Richardscurve fit
    velocity = np.diff(pos_x_uniq) / (np.diff(frames_uniq) / FPS) * -1

    # only take positve velocity values (flow from right to left)
    v_pos_ind = velocity > 0

    # match dimension of pos_x array with velocity array
    x_positions = pos_x_uniq[1:]
    x_positions = x_positions[v_pos_ind]
    velocity = velocity[v_pos_ind]

    # sorting data helpful for fit
    sort_ind = np.argsort(x_positions)
    x_positions = x_positions[sort_ind]
    velocity = velocity[sort_ind]

    return x_positions, velocity


def velocity_predict_fit_inlet(x_positions, velocity, channel_zone,
                               inlet_offset=50):
    """Predict the velocity of a particle passing the channel by fitting a
    curve to the v(x) data.

    parameters:
        x_positions: *ndarray*
            x-positions in pixels
        velocity: *ndarray*
            velocity in um/s
        channel_zone: *tuple, or list, or ndarray*
            x-positionof channel start and end
        inlet_offest: *int*
            value how many um inside the channel should be accounted to the fit
            region at inlet
    out: *array* or *bool*
        Returns array containing the fit parameters or False if
    """
    # determine velocity curve at channel inlet
    # inlet offset because steady state velocity acquired some time after inlet
    inlet_ind = x_positions > (channel_zone[1] - inlet_offset)
    x_inlet = x_positions[inlet_ind]
    v_inlet = velocity[inlet_ind]

    x_fit_inlet = x_inlet - channel_zone[1]

    if len(v_inlet > 0):
        try:
            init_gues = [min(v_inlet), max(v_inlet), v_inlet[-1], 1., 1.]
            fitparams_inlet, _ = curve_fit(RichardsCurve, x_fit_inlet,
                                           v_inlet, p0=init_gues)
        except:
            print("Inlet fitting failed")
            fitparams_inlet = False
    else:
        fitparams_inlet = False

    return fitparams_inlet


def velocity_predict_fit_outlet(x_positions, velocity, channel_zone,
                                outlet_offset=10):
    '''Predict the velocity of a particle passing the channel by fitting a 
    curve to the v(x) data.
    
    parameters:
        x_positions: *ndarray*
            x-positions in pixels
        velocity: *ndarray*
            velocity in um/s
        channel_zone: *tuple, or list, or ndarray*
            x-positionof channel start and end
        inlet_offest: *int*
            value how many um inside the channel should be accounted to the fit
            region at inlet
    out: *array* or *bool*
        Returns array containing the fit parameters or False if
    '''

    # determine velocity curve at channel inlet
    # inlet offset because steady state velocity acquired some time after inlet
    outlet_ind = x_positions < (channel_zone[0] + outlet_offset)
    x_outlet = x_positions[outlet_ind]
    v_outlet = velocity[outlet_ind]

    x_fit_outlet = x_outlet - channel_zone[0]

    if len(v_outlet > 0):
        try:
            init_gues = [min(v_outlet), max(v_outlet), v_outlet[-1], 1., 1.]
            fitparams_outlet, _ = curve_fit(RichardsCurve, x_fit_outlet,
                                            v_outlet, p0=init_gues)
        except:
            print("Outlet fitting failed")
            fitparams_outlet = False
    else:
        fitparams_outlet = False

    return fitparams_outlet


def velocity_predict_fit_channel(x_positions, velocity, channel_zone,
                                 inlet_offset=50, outlet_offset=10):
    # determine velocity in channel
    channel_ind = ((x_positions < (channel_zone[1] - inlet_offset))
                   & (x_positions > (channel_zone[0] + outlet_offset)))
    #    x_channel = x_positions[channel_ind]
    v_channel = velocity[channel_ind]

    try:
        v_channel_predict = np.median(v_channel)
    except:
        v_channel_predict = False

    v_channel_predict = np.median(v_channel)

    return v_channel_predict


def velocity_predict_fit(x_positions, velocity, channel_zone,
                         inlet_offset=50, outlet_offset=10,
                         fit_inlet=True, fit_channel=True, fit_outlet=True):
    """Predict the velocity of a particle passing the channel by fitting a
    curve to the v(x) data.

    parameters:
        x_positions: *ndarray*
            x-positions in pixels
        velocity: *ndarray*
            velocity in um/s
        channel_zone: *tuple, or list, or ndarray*
            x-positionof channel start and end
        inlet_offest, outlet_offset: *int*
            value how many um inside the channel should be accounted to the fit
            region at inlet and outlet

    out: *dictionary*
        Dict with fitparameters fot the curve at inlet and outlet, median
        velocity inside the channel and the zone of steady velocity.
    """

    # make sure channel_zone is sorted right
    if channel_zone[0] > channel_zone[1]:
        channel_zone = np.array(channel_zone)[::-1]

    # convert to m/s, fit works better for smaller values
    velocity = 1e-6 * velocity

    data_dict = {'steady_v_zone': np.array([channel_zone[0] + outlet_offset,
                                            channel_zone[1] - inlet_offset])
                 }

    if fit_inlet:
        fitparams_inlet = velocity_predict_fit_inlet(x_positions, velocity,
                                                     channel_zone,
                                                     inlet_offset=inlet_offset)
        data_dict['fit_paras_inlet'] = fitparams_inlet

    if fit_outlet:
        fitparams_outlet = velocity_predict_fit_outlet(x_positions, velocity,
                                                       channel_zone,
                                                       outlet_offset=outlet_offset)
        data_dict['fit_paras_outlet'] = fitparams_outlet

    if fit_channel:
        v_channel_predict = velocity_predict_fit_channel(x_positions, velocity,
                                                         channel_zone,
                                                         inlet_offset=50,
                                                         outlet_offset=10)
        data_dict['v_channel'] = v_channel_predict

    return data_dict


def displacement_predict(dict_vpredict, x_position, delta_t, channel_zone):
    """OBSOLETE"""

    # check position of object
    pos_inlet = dict_vpredict['steady_v_zone'][1]
    pos_outlet = dict_vpredict['steady_v_zone'][0]

    # if inside channel
    if x_position <= pos_inlet and x_position >= pos_outlet:
        v = dict_vpredict['v_channel']
    elif x_position > pos_inlet:
        fitparas_inlet = dict_vpredict['fit_paras_inlet']
        v = RichardsCurve(x_position - channel_zone[1], *fitparas_inlet)
    elif x_position < pos_outlet:
        fitparas_outlet = dict_vpredict['fit_paras_outlet']
        v = RichardsCurve(x_position - channel_zone[0], *fitparas_outlet)

        # *-1 because curve fit only works for positve velocities
    # flow from right to left: negative v values
    # v is calculated in m/s, delta_x is given in um
    displacement = v * 1e6 * delta_t * -1

    return displacement


def displacement_predict2(dict_vpredict, x_position, delta_t, channel_zone):
    """Idea: Predict delta_x by v_predict*delta_t. Then compute actual
    displacment by using v_predict(x+delta_x/2)*delta_t. This value is closer
    to the "true" velocity of the object during delta_t"""
    # check position of object
    # print(dict_vpredict)
    # print("steady zone = ", dict_vpredict['steady_v_zone'])
    pos_inlet = dict_vpredict['steady_v_zone'][1]
    pos_outlet = dict_vpredict['steady_v_zone'][0]

    # if inside channel
    if pos_inlet >= x_position >= pos_outlet and 'v_channel' in dict_vpredict:
        v = dict_vpredict['v_channel']

    # if cell before channel
    elif x_position > pos_inlet and 'fit_paras_inlet' in dict_vpredict:
        fitparas_inlet = dict_vpredict['fit_paras_inlet']
        if np.ndim(fitparas_inlet) > 0:
            v = RichardsCurve(x_position - channel_zone[1], *fitparas_inlet)
            delta_x = v * 1e6 * delta_t * -1
            v = RichardsCurve(x_position - channel_zone[1] + delta_x / 2.,
                              *fitparas_inlet)
        else:
            v = dict_vpredict['v_channel']

    # if cell after channel
    elif x_position < pos_outlet and 'fit_paras_outlet' in dict_vpredict:
        # if no curve can be predicted: use median channel velocity:
        fitparas_outlet = dict_vpredict['fit_paras_outlet']
        if np.ndim(fitparas_outlet) > 0:
            v = RichardsCurve(x_position - channel_zone[0], *fitparas_outlet)
            delta_x = v * 1e6 * delta_t * -1
            v = RichardsCurve(x_position - channel_zone[1] + delta_x / 2.,
                              *fitparas_outlet)
        else:
            v = dict_vpredict['v_channel']

    else:
        return False

    # Computation of Richardscurve might be not possible in the steady regimes
    # Can result in nan or inf value
    # --> just use steady channel velocity
    if np.isnan(v) or np.isinf(v):
        v = dict_vpredict['v_channel']

    # *-1 because curve fit only works for positive velocities
    # flow from right to left: negative v values
    # v is calculated in m/s, delta_x is given in um
    displacement = v * 1e6 * delta_t * -1

    return displacement


def find_analysis_path(measurement_file,
                       analysis_folder=r"Q:\Analysis\Experiments"):
    """Find path to the analysis file."""
    # find path to cached analysis files
    # file is read from local or external drive
    if measurement_file[:2] in ["C:", "D:", "E:"]:
        keyword = "Experiment Data\\"
        ind = measurement_file.find(keyword)
        projectfolder = measurement_file[ind + len(keyword):]
        savepath = os.path.join(analysis_folder, projectfolder)
        # file is read from MPL share
    elif (measurement_file[:2] in ["R:", "U:", "T:"] or
          "frontend.storage.int.mpl.mpg.de\\mplshare" in measurement_file or
          "frontend.storage.int.mpl.mpg.de\\guck_division2" in measurement_file):
        keyword = "Data\\"
        ind = measurement_file.find(keyword)
        projectfolder = measurement_file[ind + len(keyword):]
        savepath = os.path.join(analysis_folder, projectfolder)
    else:
        print("unkown drive {0}. Data will not be saved.".format(
            measurement_file.split("\\")[0]))
        savepath = False
        return None

    savefolder = os.path.dirname(savepath)
    if not os.path.isdir(savefolder):
        os.makedirs(savefolder)

    return os.path.splitext(savepath)[0][:-4]


def find_analysis_folder(measurement_file,
                         analysis_folder=r"Q:\Analysis\Experiments"):
    """Find path to the analysis folder."""
    # find path to cached analysis files
    # file is read from local or external drive
    if measurement_file[:2] in ["C:", "D:", "E:"]:
        keyword = "Experiment Data\\"
        ind = measurement_file.find(keyword)
        projectfolder = measurement_file[ind + len(keyword):]
        savepath = os.path.join(analysis_folder, projectfolder)
        # file is read from MPL share
    elif (measurement_file[:2] in ["R:", "U:", "T:"] or
          "frontend.storage.int.mpl.mpg.de\\mplshare" in measurement_file or
          "frontend.storage.int.mpl.mpg.de\\guck_division2" in measurement_file):
        keyword = "Data\\"
        ind = measurement_file.find(keyword)
        projectfolder = measurement_file[ind + len(keyword):]
        savepath = os.path.join(analysis_folder, projectfolder)
    else:
        print("unkown drive {0}. Data will not be saved.".format(
            measurement_file.split("\\")[0]))
        savepath = False

    savefolder = os.path.dirname(savepath)
    if not os.path.isdir(savefolder):
        os.makedirs(savefolder)

    return savefolder


def create_filename_prefix(measurement_file):
    # find path to cached analysis files
    # file is read from local or external drive
    if measurement_file[:2] in ["C:", "D:", "E:"]:
        keyword = "Experiment Data\\"
        ind = measurement_file.find(keyword)
        projectfolder = measurement_file[ind + len(keyword):]
    # file is read from MPL share
    elif measurement_file[:2] in ["R:", "U:", "T:"]:
        keyword = "Data\\"
        ind = measurement_file.find(keyword)
        projectfolder = measurement_file[ind + len(keyword):]

    # convert folder path to file name
    filename = "_".join(projectfolder.split("\\"))
    filename = os.path.splitext(filename)[0][:-4]

    return filename


def seperate_objects_dev(DATA, dataset, channel_zone,
                         returnZones=False, WindowSize=25,
                         measurement_file=None,
                         analysis_folder=r"Q:\Analysis\Experiments",
                         **kwargs):
    """Assign object number to the events detected with GetVidData.
    
    Parameters:
        DATA: *dictionary*
            DATA dictionary from GetVidData
        dataset: *object*
            dclab.dataset that was used to create DATA
        channel_zone: *list or tuple*
            Coordinates of the channel region in um
        returnZones: *bool, optional*
            If True, zones where the objects were predicted are returned
        WindowSize: *float, optional*
            Minimum size of the tolerance region in um for predicting where 
            object will be in the next frame.
        measurement_file: *string, optional*
            Full path to the .tdms or .rtdc file of the measurement that is 
            analyzed. Is used to find cache file of polygon filter
        analysis_folder: *string, optional*
            Path to folder where to find the cached analysis files like channel
            points and polygon filters. default="Q:\Analysis\Experiments"
        **kwargs: 
            conditional parameters that are passed to the Velocity curve fit 
            function and polygon filter
            
    Returns:
        out: *dictionary*
            Dictionary that contains info sorted for each object. keys are of 
            format 'Object 0', 'Object 1', ...
            If returnZones=True additionaly a dictionary with the prediction 
            zones for each frame are returned
    """
    frame_rate = dataset.config['imaging']['frame rate']

    # pos_x[i] + (med_x_displacement +/- Delta_x_displacement) creates a zone
    # where the object from frame i is expected in frame i+1
    frames = np.array(DATA['frame'])
    Index = np.array(DATA['index'])
    Pos_x = np.array(DATA['pos_x']) * 0.34  # convert back to um
    size_y = np.array(DATA['size_y'])

    frames_uniq = np.unique(frames)
    X_positions = []
    EventIndices = []
    Sizes_y = []

    # create list that contains all entries in each frame for pos_x and
    # event_index
    for frame in frames_uniq:
        ind = np.nonzero(frames == frame)[0]
        X_positions.append(Pos_x[ind])
        EventIndices.append(Index[ind])
        Sizes_y.append(size_y[ind])

    # dictionary to save object Data
    Objects = {}
    zones = {}
    # create list that contains objects that should be kept track of
    tracked_objects = [0]

    if 'fit_inlet' in kwargs:
        fit_inlet = kwargs['fit_inlet']
    else:
        fit_inlet = True
    if 'fit_outlet' in kwargs:
        fit_outlet = kwargs['fit_outlet']
    else:
        fit_outlet = True
    if 'fit_channel' in kwargs:
        fit_channel = kwargs['fit_channel']
    else:
        fit_channel = True
    if 'inlet_offset' in kwargs:
        inlet_offset = kwargs['inlet_offset']
    else:
        inlet_offset = 50
    if 'outlet_offset' in kwargs:
        outlet_offset = kwargs['outlet_offset']
    else:
        outlet_offset = 10

    print("Object tracking")
    for frame_ind in trange(len(frames_uniq)):
        # values from the velocity prediction
        # recalculate every 1000 frames

        if 'refit_interval' in kwargs:
            i_refit = kwargs['refit_interval']
        else:
            i_refit = 1000

        if frame_ind % i_refit == 0:
            lastFrame = frame_ind + i_refit
            if lastFrame < len(frames_uniq):
                lastFrame = len(frames_uniq)

            x_positions, velocity = prep_velocityFit(dataset,
                                                     first_frame=frame_ind,
                                                     last_frame=lastFrame)

            # only need polygon filter for inlet or outlet fitting
            if fit_inlet or fit_outlet:
                if 'polypath' in kwargs:
                    polypath = kwargs['polypath']
                else:
                    # preferable save polygon file in folder one level above the
                    # measurement files
                    measurement_file = dataset.path
                    mpath = os.path.splitext(measurement_file)[0]
                    msplit = mpath.split("\\")
                    polyfilename = msplit[-2] + "_" + msplit[-1][:4] + ".poly"
                    polyfolder = "\\".join(mpath.split("\\")[:-2])
                    polypath = os.path.join(polyfolder, polyfilename)

                if not os.path.isfile(polypath):
                    ind_PolyFilter, points_PolyFilter = polyf.get_PolyFilterVelocity(x_positions, velocity,
                                                                                     channel_zone,
                                                                                     inlet_offset=inlet_offset,
                                                                                     outlet_offset=outlet_offset,
                                                                                     returnPoly=True)
                    np.savetxt(polypath, points_PolyFilter)
                else:
                    points_PolyFilter = np.loadtxt(polypath)
                    ind_PolyFilter = polyf.get_PolyFilterVelocity(x_positions, velocity,
                                                                  channel_zone,
                                                                  inlet_offset=inlet_offset,
                                                                  outlet_offset=outlet_offset,
                                                                  createPoly=False,
                                                                  returnPoly=False,
                                                                  points_PolyFilter=points_PolyFilter)

                x_positions = x_positions[ind_PolyFilter]
                velocity = velocity[ind_PolyFilter]

            # Always perform the fitting for the first frame
            if frame_ind == 0:
                v_dict = velocity_predict_fit(x_positions, velocity,
                                              channel_zone,
                                              fit_channel=fit_channel,
                                              fit_inlet=fit_inlet,
                                              fit_outlet=fit_outlet,
                                              inlet_offset=inlet_offset,
                                              outlet_offset=outlet_offset)

            # If fitting fails: don't update the fit parameters
            else:
                v_dict_new = velocity_predict_fit(x_positions, velocity,
                                                  channel_zone,
                                                  fit_channel=fit_channel,
                                                  fit_inlet=fit_inlet,
                                                  fit_outlet=fit_outlet,
                                                  inlet_offset=inlet_offset,
                                                  outlet_offset=outlet_offset)

                v_dict['steady_v_zone'] = v_dict_new['steady_v_zone']
                if fit_channel:
                    v_dict['v_channel'] = v_dict_new['v_channel']

                # Failed fitting results in v_dict['fit_paras_inlet']=False
                # principle: np.ndim(False)=0, np.ndim([])=1
                if fit_inlet:
                    if np.ndim(v_dict_new['fit_paras_inlet']) > 0:
                        v_dict['fit_paras_inlet'] = v_dict_new['fit_paras_inlet']
                if fit_outlet:
                    if np.ndim(v_dict_new['fit_paras_outlet']) > 0:
                        v_dict['fit_paras_outlet'] = v_dict_new['fit_paras_outlet']

        if frame_ind == 0:
            stop_tracking = []
        else:
            for obj_rmv in stop_tracking:
                tracked_objects.remove(obj_rmv)
            stop_tracking = []

        frame = frames_uniq[frame_ind]

        zones[str(frame)] = []
        # Iterate over events in the frame
        for i in range(len(EventIndices[frame_ind])):
            Ind = EventIndices[frame_ind][i]
            X = X_positions[frame_ind][i]
            y_size = Sizes_y[frame_ind][i]

            if frame_ind == 0:
                key = "Object 0"
                Objects[key] = [(frame, Ind, X, y_size)]
            else:
                Found = False
                for tr_obj in tracked_objects:
                    key = "Object " + str(tr_obj)
                    prev_X = Objects[key][-1][2]
                    prev_frame = Objects[key][-1][0]

                    delta_t = (frame - prev_frame) / frame_rate
                    # displacement = displacement_predict(v_dict, prev_X,
                    displacement = displacement_predict2(v_dict, prev_X,
                                                         delta_t, channel_zone)
                    # stop tracking the object if error in displacement fct
                    # occurs --> object likely left the channel
                    if displacement == False:
                        stop_tracking.append(tr_obj)
                        continue

                    predZoneStart = prev_X + displacement - WindowSize / 2.
                    predZoneEnd = prev_X + displacement + WindowSize / 2.
                    prediction_zone = (predZoneStart, predZoneEnd)

                    #                    if not np.isnan(displacement):
                    zones[str(frame)].append([tr_obj, prediction_zone])

                    # check if prediction zone is outside the channel
                    if prediction_zone[1] < 0:
                        stop_tracking.append(tr_obj)
                    else:
                        if X > prediction_zone[0] and X < prediction_zone[1]:
                            Objects[key].append((frame, Ind, X, y_size))
                            Found = True

                if Found == False:
                    new_obj_nr = len(Objects)
                    key = "Object " + str(new_obj_nr)
                    tracked_objects.append(new_obj_nr)
                    Objects[key] = [(frame, Ind, X, y_size)]

                # list might contain same element twice --> removing procedure
                # causes error --> get rid of duplicates
                stop_tracking = list(np.unique(stop_tracking))

    if returnZones:
        return Objects, zones
    else:
        return Objects


def create_channelpts_and_polyfilter(dataset,
                                     first_frame=0, last_frame=0,
                                     **kwargs):
    """Create channelpts polygonfilter files to use for further analysis"""
    PIX_SIZE = dataset.config['imaging']['pixel size']
    measurement_file = str(dataset.path)
    if 'channelpath' in kwargs:
        detection_zone = get_channel_pts(measurement_file,
                                         channelpath=kwargs['channelpath'])
    else:
        detection_zone = get_channel_pts(measurement_file)
    channel_zone = np.array(detection_zone) * PIX_SIZE

    size_ds = len(dataset['frame'])
    if last_frame == 0 or last_frame > size_ds:
        last_frame = size_ds

    x_positions, velocity = prep_velocityFit(dataset,
                                             first_frame=first_frame,
                                             last_frame=last_frame)

    if 'polypath' in kwargs:
        polypath = kwargs['polypath']
    else:
        polypath = (find_analysis_path(measurement_file)
                               + "polyfilter.poly")

    if not os.path.isfile(polypath):
        ind_PolyFilter, points_PolyFilter = polyf.get_PolyFilterVelocity(x_positions, velocity, channel_zone,
                                                                         returnPoly=True)
        np.savetxt(polypath, points_PolyFilter)
