# -*- coding: utf-8 -*-
"""
Created on Fri Feb 15 21:04:28 2019

@author: s8570974

Functions to distinguish bw different object that where captured multiple times
"""
import sys
import numpy as np
#from itertools import chain
import cv2
import os
from matplotlib.pyplot import cm
from imageio import get_writer#, get_reader
from tqdm import tqdm
import track, vid

#### funtions to calculate flow properties ####
#sys.path.append(r"C:\Users\s8570974\py-packages\py36\pyrtdc")
#from pyrtdc.theory import flow_velocity

if __name__ == "__main__":
    import dclab 
    sys.path.append(r'C:\Users\s8570974\py-packages\py36')
    from pyrtdc.misc import define_rect
    
#    in_path = r"E:\20190327_Felix_deformation_variation_steadystate\20um_0.05MC"
#    in_path = r"E:\20180628_Felix_HL60_Dynamics\20180628_S3_20um"
#    in_path = r"E:\20190628_Felix_Coacervates_extensionalFlow\PDDA_2to1_ATP_10um"
    in_path = r"F:\Experiment Data\20190627_Felix_Beads_trajectory\PAA_S03_NHS_1.1"
    measurement = 6
    
#    mno_string = "M" + str(1000 + measurement)[1:]
#    filename = mno_string + "_data.rtdc"

    mno_string = "M" + str(measurement)
    filename = mno_string + "_data.tdms"
    
    in_file = in_path + "\\" + filename    
    tsv_name = in_path.split("\\")[-1] + "_" + mno_string + "_track_results.tsv"
    
    #set filters
    areamin = 50
    areamax = 250
    aratio_max = 1.05
    
    first_frame = 0    #must be int
    last_frame = 2000  #must be int

    if in_file.endswith('.tdms'):
        imfile = in_path + "\\" + mno_string + "_bg_102.png"
    elif in_file.endswith('.rtdc'):
        imfile = in_path + "\\" + mno_string + "_bg.png"
    
    img = cv2.imread(imfile)
    img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    img_resize = 1.
    rect_pts = define_rect(img, resize = img_resize)
    
    detection_zone = [rect_pts[0][0]/img_resize, rect_pts[1][0]/img_resize]
    detection_zone.sort()
    
    #read in experiment file and filter
    ds = dclab.new_dataset(in_file)
    
    ds.config["filtering"]["area_um min"] = areamin
    ds.config["filtering"]["area_um max"] = areamax
    ds.config["filtering"]["area_ratio max"] = aratio_max
    ds.config["filtering"]["frame min"] = ds['frame'][first_frame] 
    ds.config["filtering"]["frame max"] = ds['frame'][last_frame]
    ds.apply_filter()  # this step is important!
    
    ds_filtered = dclab.new_dataset(ds)
    
    #get data in format to be used with tracking functions
    DATA = {}
    
#    DATA["frame"] = ds_filtered["frame"]
#    DATA["index"] = ds_filtered["index"] - 1 #dclab index starts at 1
#    DATA["index_unfiltered"] = ds['index'][ds.filter.all] - 1
#    DATA["area"] = ds_filtered["area_um"] 
#    DATA["deformation"] = ds_filtered["deform"]
#    DATA["area ratio"] = ds_filtered["area_ratio"]
#    DATA["length"] = ds_filtered["size_x"]
#    DATA["height"] = ds_filtered["size_y"]
#    DATA["pos_x"] = ds_filtered["pos_x"] / 0.34 #pos_x in pixel
#    DATA["pos_y"] = ds_filtered["pos_y"]
#    DATA["inertia_ratio"] = ds_filtered["inert_ratio_cvx"]
#    DATA["inertia_ratio_raw"] = ds_filtered["inert_ratio_raw"]
#    DATA["contour_raw"] = ds_filtered["contour"]
##    DATA["conv_hull"] = ds_filtered["contour"]
#    DATA['image'] = list(ds_filtered["image"])
#    
#    for key in DATA.keys():
#        DATA[key] = list(DATA[key])
    print("Prepare DATA dictionary")
    for feat in tqdm(ds_filtered.features):
        if not (feat=='image' or feat=='contour'
                or feat=='mask' or feat=='trace'
                or feat=='inert_ratio_prnc' or feat=='tilt'
                or feat=='volume' or feat=='emodulus'):
            DATA[feat] = ds_filtered[feat]
            
    DATA["index"] = DATA["index"] - 1 #dclab index starts at 1
    DATA["index_unfiltered"] = ds['index'][ds.filter.all] - 1
    DATA["pos_x"] = ds_filtered["pos_x"] / 0.34 #pos_x in pixel
    DATA["pos_y"] = ds_filtered["pos_y"] / 0.34 #pos_y in pixel
#    print("preparing image data. please wait a moment")
#    DATA['image'] = ds_filtered["image"]
        
    PIX_SIZE = ds_filtered.config['imaging']['pixel size']
    FPS = ds_filtered.config['imaging']['frame rate']
    flowrate = ds_filtered.config['setup']['flow rate']
    channel_width = ds_filtered.config['setup']['channel width']

#    Objects = seperate_objects(DATA)#, WindowSize=100.)
    Objects = track.seperate_objects_new(DATA, flowrate, channel_width,
                                         np.array(detection_zone)*PIX_SIZE,FPS,
                                         WindowSize=25)
    
    DATA, ObjectsData = track.get_ObjectData(Objects, DATA)
    
    DATA, ObjectsData = track.get_VelocityAndTime(ObjectsData, DATA, detection_zone,
                                            FPS, PIX_SIZE)
    
    DATA_filtered, ObjectsData = track.filter_ObjectsDATA(ObjectsData, DATA, detection_zone,
                                       min_events=3)
    
    DATA_filtered = track.sort_DATA_by_objNr(DATA_filtered)
    
    track.DataToTSV(DATA_filtered,tsv_name)
    
    vid_path="Videos\\sepCells_avi.avi"
#    VideoWriteSepObjects(ds, DATA, FirstFrame=0, LastFrame=200,
    vid.VideoWriteSepObjects_dclab(ds, DATA_filtered,
                         FirstFrame=first_frame, LastFrame=last_frame,
                         save_vidfile = vid_path, macro_block_size=16)