# -*- coding: utf-8 -*-
"""
Created on Fri Feb 15 20:42:36 2019

@author: s8570974
"""

import numpy as np
import scipy.fftpack

from .ContFeatures import centroid_polygon

def interpolate_contour(cont,points=100):
    '''Linear interpolation bw contour points in xy-plane.
    points is the number of points interpolated bw 2 contour points.
    Returns interpolated contour as: x_int, y_int'''
    
    x_cont = cont[:,0]
    y_cont = cont[:,1]
    x_diff = np.diff(x_cont)
    y_diff = np.diff(y_cont)
    
    #perform interpolation bw all contour points individual
    #and stack the result to array
    #THERE IS PROBABLY A MORE ELEGANT WAY TO SOLVE THIS
    t = np.linspace(0,1,points,endpoint=False)
    for i in range(len(x_diff)):
        if i == 0:
            x_int = np.hstack((x_cont[i] + x_diff[i]*t,x_cont[i+1]))
            y_int = np.hstack((y_cont[i] + y_diff[i]*t,y_cont[i+1]))
        else:
            x_int = np.hstack((x_int[:-1],x_cont[i] + x_diff[i]*t))
            y_int = np.hstack((y_int[:-1],y_cont[i] + y_diff[i]*t))

    x_int = np.hstack((x_int[:-1],x_cont[-1] + (x_cont[0]-x_cont[-1])*t))
    y_int = np.hstack((y_int[:-1],y_cont[-1] + (y_cont[0]-y_cont[-1])*t))

    return x_int, y_int

def polar_transform(contour, sort=True, sort_index=False):
    '''Transform xy-contour to polar coordinates.
    Also sorts data for phi
    Returns r, phi 
            - 0<= phi <2*pi
    
    if sort_index=True: 
        return: r, phi, sort_index'''

    #shift contour to point of origin coordinates
    x_com, y_com = centroid_polygon(contour)
    x_cont = contour[:,0] - x_com
    y_cont = contour[:,1] - y_com
    
    #polar coordinates
    r = np.sqrt(x_cont**2+y_cont**2)
    phi = np.arctan2(y_cont, x_cont)
    #arctan2 -> -pi<phi<pi
    #0 <= phi < 2*pi:
    phi = phi%(2*np.pi)
    
    if sort:
        #sort by phi
        sort_idx = np.argsort(phi)
        phi = phi[sort_idx]
        r = r[sort_idx] 
    
    if sort==True and sort_index==True:
        return r, phi, sort_idx
    else:
        return r, phi

def polar_backtransform(phi, r):
    '''Transform polar coordinate data (phi,r) to 
    cartesian coordinates (x,y).
    
    returns:
        out: *tuple* (x,y)
    '''
    x = r * np.cos(phi)
    y = r * np.sin(phi)
    
    return x,y

def Fouriertrans_Fregin(contour,max_order=9,points=100,norm=False):
    """Perform the Fourier transform like described in 
    Fregin et al., Nat. Commun., 2019
    
    a_k[k] = np.sum(r*np.cos(k*phi) * delta_phi) / np.pi
    b_k[k] = np.sum(r*np.sin(k*phi) * delta_phi) / np.pi
    
    max_order: highest order k of the Fourier transform to compute
    points: number of points to be used in the contour interpolation
    norm: if True: all values are normed on the zero order a_k[0]
    
    returns:
        out: *tuple* (a_k, b_k)
    """    
    
    #interpolate contour points
    x_int, y_int = interpolate_contour(contour,points=points)
    cont_int = np.vstack((x_int, y_int)).T
    
    #transform to polar coordinates
    r, phi = polar_transform(cont_int)
    
    a_k = np.zeros(max_order+1)
    b_k = np.zeros(max_order+1)

    #compute delta phi
    delta_phi = np.diff(np.append(phi,phi[0]))%(2*np.pi)
    delta_phi = 0.5 * (delta_phi + np.append(delta_phi[-1],delta_phi[:-1]))
    
    for k in range(max_order+1):
        a_k[k] = np.sum(r*np.cos(k*phi) * delta_phi) / np.pi
        b_k[k] = np.sum(r*np.sin(k*phi) * delta_phi) / np.pi
    
    if norm==True:
        a_k = a_k/a_k[0]
        b_k = b_k/a_k[0]
        
    return a_k, b_k

def contour_reconstruction_Fregin(a_k, b_k, points=200):
    '''Contour reconstruction from Fourier descriptos a_k, b_k 
    in polar coordinates as described in 
    Fregin et al., Nat. Commun., 2019.
    
    points: number of points in reconstructed contour
    
    returns:
        out: *tuple* (phi_rec, r_rec)
        phi_rec: *array*; angle (uniform distribution -pi<phi<pi)
        r_rec: *array*; reconsructed distance per angle
    '''
    
    phi_rec = np.linspace(-np.pi,np.pi,points, endpoint=False)
    
    r_rec = [a_k[0]/2. + np.sum(a_k[1:]*np.cos(np.arange(1,len(a_k))*phi) 
                                + b_k[1:]*np.sin(np.arange(1,len(b_k))*phi))
             for phi in phi_rec]
    
    return phi_rec, np.array(r_rec)

def contour_reconstruction_Fregin_odd(a_k, b_k, points=200):
    '''Contour reconstruction from odd Fourier descriptos ak[2k+1], bk[2k+1] 
    in polar coordinates as described in Fregin et al., Nat. Com, 2019.
    
    points: number of points in reconstructed contour
    
    returns:
        out: *tuple* (phi_rec, r_rec)
        phi_rec: *array*; angle (uniform distribution -pi<phi<pi)
        r_rec: *array*; reconsructed distance per angle
    '''
    
    phi_rec = np.linspace(-np.pi,np.pi,points, endpoint=False)
    
    r_rec = [a_k[0]/2. + np.sum(a_k[1::2]*np.cos(np.arange(1,len(a_k),2)*phi) 
                               + b_k[1::2]*np.sin(np.arange(1,len(b_k),2)*phi))
             for phi in phi_rec]
    
    return phi_rec, np.array(r_rec)

def contour_reconstruction_Fregin_even(a_k, b_k, points=200):
    '''Contour reconstruction from even Fourier descriptos ak[2k], bk[2k] 
    in polar coordinates as described in Fregin et al., Nat. Com, 2019.
    
    points: number of points in reconstructed contour
    
    returns:
        out: *tuple* (phi_rec, r_rec)
        phi_rec: *array*; angle (uniform distribution -pi<phi<pi)
        r_rec: *array*; reconsructed distance per angle
    '''
    
    phi_rec = np.linspace(-np.pi,np.pi,points, endpoint=False)
    
    r_rec = [a_k[0]/2. + np.sum(a_k[2::2]*np.cos(np.arange(2,len(a_k),2)*phi) 
                               + b_k[2::2]*np.sin(np.arange(2,len(b_k),2)*phi))
             for phi in phi_rec]
    
    return phi_rec, np.array(r_rec)

def interpolate_contour_polar(contour, points=200, projection='cartesian'):
    '''Interpolate contour points linearly in cartesian coordinates
    with equally spaced angles in polar space. Works only for convex 
    contours.
    
    Inputs:
        - contour: contour in xy
        - points, (optional): number of points in the interpolated contour
            default = 200
        - projection, (optional): return result in xy or polar coordinates
            options: 'cartesian', 'polar'
            
    Return: 
        out: *tuple* (x_int, y_int) or (r_int, phi_int)
        
    Implementation idea:
    Define line y=m*x+n bw two beighboring points of the contour for all 
    contour points. Find the interscept of the line from the centroid of 
    the contour, for a range of equally spaced angles, with the contour. 
    '''

    #shift contour to point of origin coordinates
    x_com, y_com = centroid_polygon(contour)
    x_cont = contour[:,0] - x_com
    y_cont = contour[:,1] - y_com

    #define angle distribution for the interpolation
    angles = np.linspace(0,2*np.pi, 200, endpoint=False)

    #polar transform original contour
    r_orig, phi_orig, sortidx = polar_transform(contour, sort_index=True)

    #sort contourpoints by phi
    x_cont = x_cont[sortidx]
    y_cont = y_cont[sortidx]

    #enclose contour
    x_cont = np.append(x_cont, x_cont[0])
    y_cont = np.append(y_cont, y_cont[0])
    r_orig = np.append(r_orig, r_orig[0])
    phi_orig = np.append(phi_orig, phi_orig[0])

    #calculate slopes bw contour points
    m = np.diff(y_cont) / np.diff(x_cont)

    #calculate y-intersect of line bw two contour points
    n = y_cont[:-1] - m*x_cont[:-1]

    x_int = np.zeros(len(angles))
    y_int = np.zeros(len(angles))

    for idx in range(len(angles)):

        #find the line for which to solve the intersect for
        #if angles[idx]>phi_orig[i] -> use line bw (x[i],y[i])-(x[i+1],y[i+1])
        #angles[idx]>phi_orig[i] can be True for more than one value
        #--> take last value=next largest angle
        slope_idx = np.nonzero(angles[idx]>phi_orig[:-1])[0]
        if len(slope_idx)>0:
            slope_xy = m[slope_idx[-1]]
            isct_y = n[slope_idx[-1]]
        #if angle[idx]<min(phi_orig) --> take line bw (x[-1],y[-1])-(x[0],y[0])
        else:
            slope_xy = m[-1]
            isct_y = n[-1]

        #slope defined by the angles[idx]
        slope_angle = np.tan(angles[idx])

        #solve for x-interscept
        if slope_xy in [-np.inf,np.inf]:
        # if len(slope_idx)>0:
            x_isc = x_cont[slope_idx[-1]]
        else:
            x_isc = isct_y/(slope_angle-slope_xy)

        #solve for y-interscept
        y_isc = slope_angle*x_isc

        x_int[idx] = x_isc
        y_int[idx] = y_isc
 
    if projection=='cartesian':
        return x_int, y_int

    elif projection=='polar':
        cont_int = np.vstack((x_int, y_int)).T
        return polar_transform(cont_int)
        
    
def DFT_contour(contour, number_of_comp=101, points=200, normalize=False,
                norm=None):
    '''Compute the (real) fast discrete fourier transform of an input contour
    using numpy.fft.rfft.
    Returns the first number_of_comp coefficients of the DFT (complex array). 
    Contour points are converted to polar coordinates r(phi) for points angles.
    Contour can be reconstructed with numpy.fft.irfft(fft_coeffs). 
    
    Inputs: 
        - contour: array of shape (x,2) with stored xy - contour points
        - number_of_comp: order until which fft coefficients should be returned
            
        - points: number of points in the interpolated contour
        - normalize: True of False, normalize contour values on zero order value
        - norm: norm normalization of the fft; default=None, other option: "ortho"
        
    Remark: real fft only computes 1st half of the coeffiecients necessary for 
    reconstruction. The rest can be computed by taking the complex conjugate of 
    the Coeffs[1:] and adding them palindromic [a[0],a[1],a[2],...,
    conj(a[2]),conj(a[1])]  (numpy.flipud(numpy.conj(fft_coeffs[1:])))
    
    Dependencies: numpy, PolarContour()'''

    r, phi = interpolate_contour_polar(contour, points=points,
                                       projection='polar')
    # do fft
    fft_coeffs = np.fft.rfft(r, norm=norm)

    if normalize==True:
        fft_coeffs=fft_coeffs/fft_coeffs[0].real

    return fft_coeffs[:number_of_comp], phi

def backtransform_np_rfft(fft_coeffs, len_input, cont_points=200):
    '''Correctly perform backtransform of fourier coefficients computed
    by np.fft.rfft(a). For this, it is necessary to know the length of the 
    input vector a.
    
    Inputs:
        - fft_coeffs: array; complex fourier coefficients of a real fft
        - len_input: length of the input vector of the original fft
        - cont_points: number of points that will be computed
        
    Returns: 
        out: *tuple* ifft_coeffs, angles
            ifft_coeffs: coefficients of the inverse fft
            angles: angle distribution of the output vector 
    '''
    angles = np.linspace(0, 2*np.pi, cont_points, endpoint=False)
    
    coeffs = np.zeros(len_input)*(1+1j)
    conj_coeffs = np.conj(fft_coeffs[1:][::-1])
    
    coeffs[:len(fft_coeffs)] = fft_coeffs
    coeffs[len_input-len(conj_coeffs):] = conj_coeffs
    N = float(len(coeffs))
    
    ifft_coeffs = [np.sum(coeffs*np.exp(1j*np.arange(len(coeffs))*phi)) / N 
                   for phi in angles] 
    
    return np.array(ifft_coeffs), angles

def backtransform_np_rfft_odd_only(fft_coeffs, len_input, cont_points=200):
    '''Correctly perform backtransform of fourier coefficients computed
    by np.fft.rfft(a) but only use the odd contours. For this, it is necessary
    to know the length of the input vector a.
    
    Inputs:
        - fft_coeffs: array; complex fourier coefficients of a real fft
        - len_input: length of the input vector of the original fft
        - cont_points: number of points that will be computed
        
    Returns: 
        out: *tuple* ifft_coeffs, angles
            ifft_coeffs: coefficients of the inverse fft
            angles: angle distribution of the output vector 
    '''
    angles = np.linspace(0, 2*np.pi, cont_points, endpoint=False)
    
    coeffs = np.zeros(len_input)*(1+1j)
    coeffs_odd = fft_coeffs.copy
    coeffs_odd[1::2] = 0+0j
    conj_coeffs = np.conj(coeffs_odd[1:][::-1])
    
    coeffs[:len(fft_coeffs)] = coeffs_odd
    coeffs[len_input-len(conj_coeffs):] = conj_coeffs
    N = float(len(coeffs))
    
    ifft_coeffs = [np.sum(coeffs*np.exp(1j*np.arange(len(coeffs))*phi)) / N 
                   for phi in angles] 
    
    return np.array(ifft_coeffs), angles

def backtransform_np_rfft_even_only(fft_coeffs, len_input, cont_points=200):
    '''Correctly perform backtransform of fourier coefficients computed
    by np.fft.rfft(a) but only use the odd contours. For this, it is necessary
    to know the length of the input vector a.
    
    Inputs:
        - fft_coeffs: array; complex fourier coefficients of a real fft
        - len_input: length of the input vector of the original fft
        - cont_points: number of points that will be computed
        
    Returns: 
        out: *tuple* ifft_coeffs, angles
            ifft_coeffs: coefficients of the inverse fft
            angles: angle distribution of the output vector 
    '''
    angles = np.linspace(0, 2*np.pi, cont_points, endpoint=False)
    
    coeffs = np.zeros(len_input)*(1+1j)
    coeffs_even = fft_coeffs.copy
    coeffs_even[2::2] = 0+0j
    conj_coeffs = np.conj(coeffs_even[1:][::-1])
    
    coeffs[:len(fft_coeffs)] = coeffs_even
    coeffs[len_input-len(conj_coeffs):] = conj_coeffs
    N = float(len(coeffs))
    
    ifft_coeffs = [np.sum(coeffs*np.exp(1j*np.arange(len(coeffs))*phi)) / N 
                   for phi in angles] 
    
    return np.array(ifft_coeffs), angles

def DCT_contour(contour,number_of_comp=101, points=200, norm=False):
    '''Compute the (real) fast discrete cosinus transform of a input contour.
    Returns the first number_of_comp coefficients of the DCT (complex array). 
    Contour points are converted to polar coordinates r(phi) for points angles.
    Contour can be reconstructed with numpy.fft.idct(dct_coeffs). 
    
    Inputs: 
        - contour: array of shape (x,2) with stored xy - contour points
        - number_of_comp: order until which fft coefficients should be returned
        - points: number of points in the interpolated contour
        - norm: True of False, norm contour values on dct_coeffs[0]
        
    Remark: real fft only computes 1st half of the coeffiecients necessary for 
    reconstruction. The rest can be computed by taking the complex conjugate of 
    the Coeffs[1:] and adding them palindromic [a[0],a[1],a[2],...,
    conj(a[2]),conj(a[1])]  (numpy.flipud(numpy.conj(fft_coeffs[1:])))
    
    Dependencies: numpy, PolarContour()'''

    r, phi = interpolate_contour_polar(contour, points=points, projection='polar')
    # do dct
    dct_coeffs = scipy.fftpack.dct(r)
    
    if norm == True:
        dct_coeffs = dct_coeffs/dct_coeffs[0].real
     
    return dct_coeffs[:number_of_comp], phi
