import numpy as np
import os
import cv2
from matplotlib.pyplot import cm
import sys
from nptdms import TdmsFile
import scipy.interpolate
import scipy.fftpack
import imageio
from itertools import chain
#from scipy import spatial
#import copy
#import cPickle
#import time
#
#start_time = time.time()

#video file to analyze
#in_file = r"C:\Users\s8570974\ownCloud\PhD\Code_general\VideoAnalysis\SampleData\M1_imaq.avi"
in_file = r"Y:\experiments_18_19\20180628_Felix_HL60_Dynamics\20180628_S3_20um\M2_imaq.avi"

in_file_split = in_file.split("\\")
#in_file_split = in_file.split("/")

#set name of save file 
#tsv_name = in_file.split("\\")[-1]
tsv_name = "_".join((in_file_split[-2],in_file_split[-1]))
tsv_name = tsv_name.strip('_imaq.avi')
tsv_name += ".tsv"

FLOW = 1

#number of frames used to initialize the backgroundimage/weight for running avg
avg_frames = 200

# Apply convexhull on contours
take_ConvHull = True
binaryops = 5

trig_thresh = 10

#size ranges for the cell in um
HEIGHT_MIN = 5
HEIGHT_MAX = 19.5
WIDTH_MIN = 5
WIDTH_MAX = 50
AREA_MIN = 50
AREA_MAX = 300
def_min = 0.
def_max = 0.4
aratio_min = 1.
aratio_max = 1.15

first_frame = 8000
last_frame = 8500

bg_threshold = 4 

macro_block_size = 16

WindowSize = 25

VideoDir = ".\\Offline"
#field in which objects should be detected, (x_min, x_max) in Pixel
#detection_zone = (50,970)

### functions to initialize data recording ###

def define_rect(image, resize=1):
    """
    Define a rectangular window by click and drag your mouse.

    Parameters
    ----------
    image: Input image.
    """
    global rect_pts, drawing, clone

    drawing = False
    image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)
    image = cv2.resize(image, None, fx = resize, fy = resize,
                       interpolation = cv2.INTER_CUBIC)
    clone = image.copy()
    rect_pts = [] # Starting and ending points
    win_name = ("Mark Channel with rectangle. Confirm selection with Enter. "
                "Redo selection by pressing 'r'") # Window name

    def select_points(event, x, y, flags, param):

        global rect_pts, drawing, clone
        if event == cv2.EVENT_LBUTTONDOWN and not rect_pts:
#        if (event == cv2.EVENT_LBUTTONDOWN and not rect_pts 
#                                                or len(rect_pts) == 2):
            rect_pts = [(x, y)]
            drawing = True
            
        elif event == cv2.EVENT_MOUSEMOVE:
            #constantly update rectangle while mouse moves
            if drawing == True:
                clone = image.copy()
                cv2.rectangle(clone, rect_pts[0], (x,y), (0, 0, 255), 1)
                cv2.imshow(win_name, clone)
                
        elif event == cv2.EVENT_LBUTTONDOWN and len(rect_pts) == 1:
            rect_pts.append((x, y))
            drawing = False

            # draw a rectangle around the region of interest
            cv2.rectangle(clone, rect_pts[0], rect_pts[1], (0, 255, 0), 2)
            cv2.imshow(win_name, clone)

    cv2.namedWindow(win_name)
    cv2.setMouseCallback(win_name, select_points)

    while True:
        # display the image and wait for a keypress
        cv2.imshow(win_name, clone)
        key = cv2.waitKey(0) & 0xFF

        if key == ord("r"): # Hit 'r' to replot the image
            clone = image.copy()
            rect_pts = []
            
        elif key == 13:  #Hit <Enter> to confirm the selection
            break

    # close the open windows
    cv2.destroyWindow(win_name)

    return rect_pts

def circularity(contour):
    """ gives back the circularity of a contour
    It relates the ratio of sqrt(Area) and perimeter to the
    ratio a ideal circle has.
    """
    area = cv2.contourArea(contour[0])
    length = cv2.arcLength(contour[0], True)
    return 2.0*np.sqrt(np.pi*area)/length

def get_frame(in_video,frame_nr):
    """"Get image from video at frame frame_nr. in_video must be opened before 
    with in_video = cv2.VideoCapture(in_file)"""
    
#    in_video.set(cv2.CAP_PROP_POS_FRAMES, frame_nr) 
#    frame_flag, frame_pure = in_video.read()
    videoReader = imageio.get_reader(in_video)
    frame_pure = videoReader.get_data(frame_nr)
    # If p.FLOW flag == 0 mirror the image
    # flow is then always from the right to the left
    # change frame to gray scale image
    if FLOW:
        if len(frame_pure.shape) > 2:   
            frame = cv2.cvtColor(frame_pure, cv2.COLOR_BGR2GRAY)
        else:
            frame = frame_pure            
    else:
        if len(frame_pure.shape) > 2:   
            frame = cv2.cvtColor(frame_pure, cv2.COLOR_BGR2GRAY)[:,::-1]
        else:
            frame = frame_pure[:,::-1]

    videoReader.close()
#    return frame_flag, frame            
    return frame            

def get_average(in_video, Start_Frame = 0, FRAMES=100):
    """ get an average image out of the FRAMES frames starting from Start_Frame
    this is needed later to subtract the background from the picture
    
    returns backgroundimage and standard deviation of one pixel during the first
    frames
    """
#    print 'average over {0} frames...'.format(FRAMES)
#    in_video.set(cv2.CAP_PROP_POS_FRAMES, Start_Frame)
#    vid_length = int(in_video.get(cv2.CAP_PROP_FRAME_COUNT))

    videoReader = imageio.get_reader(in_video)
    vid_length = videoReader.get_meta_data()['nframes']
    
    if Start_Frame + FRAMES > vid_length:
        FRAMES = vid_length - Start_Frame

#    vid_width = int(in_video.get(cv2.CAP_PROP_FRAME_WIDTH))
#    vid_height= int(in_video.get(cv2.CAP_PROP_FRAME_HEIGHT))
#    framesize = (vid_height,vid_width)
    framesize = videoReader.get_meta_data()['size'][::-1]
    
    summe = np.zeros(framesize,dtype=np.float64)
    noise = np.zeros(FRAMES)
    for i in xrange(FRAMES):
#        in_video.grab()
#        frame_flag, frame = in_video.retrieve()
        frame = videoReader.get_data(Start_Frame + i)
#        if not frame_flag:
#            print 'Error during averaging the first {0} Frames'.format(FRAMES)
#            quit()    
#            sys.stderr.write('\r#bearbeite Frame pos: {0}  '.format(in_video.get(cv2.CAP_PROP_POS_FRAMES)))
        if len(frame.shape) > 2:   
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        else:
            gray = frame            
        cv2.accumulate(gray,summe)
        noise[i] = gray[2,int(framesize[1]/2)]
    mittel = np.zeros(framesize, dtype = np.uint8)
    cv2.convertScaleAbs(summe, mittel, 1.0/FRAMES)
#    cv2.imwrite(p.DIR+'/average_'+p.out_file+'.png',mittel)
    if FLOW:
        AVERAGE = mittel
    else:   # Mirror if flow comes from the right
        AVERAGE = mittel[:,::-1]
#    in_video.set(cv2.CAP_PROP_POS_FRAMES, 0)
    return AVERAGE, np.std(noise)

def img_proc(src, avg, thresh_m = 'absdiff', threshold = 6, threshold2 = 180, 
             binary_ops=5):
    """ Process image for contour finding
    A background image is subtracted and a threshold applied.
    There are two possibities for the background subtraction: abs(src-bg) or
    (src-bg) the latter can lead to negative values which are mapped into values
    just below 255. Those pixels have to be eliminated by an addition thresh
    step.
    img_proc_abs uses the absolute difference """

    #if p.blur > 0:
    #    sigma = 2*int(p.blur/2)+1
    #    tmp1 =cv2.GaussianBlur(src, (21,21), sigma)
    #    src = tmp1
    #    tmp2 = cv2.GaussianBlur(avg, (21,21), sigma)
    #    avg = tmp2
    thresh = src    
    if thresh_m == 'absdiff':
        diff = cv2.absdiff(src,avg)
        _, thresh = cv2.threshold(diff, threshold, 255, cv2.THRESH_BINARY)
    if thresh_m == 'absdiff_blur':
        diff = cv2.absdiff(src,avg)
        smooth = cv2.medianBlur(diff,7)
        _, thresh = cv2.threshold(smooth, threshold, 255, cv2.THRESH_BINARY)
    if thresh_m == 'simple':
        diff = src-avg
        _, thresh_1 = cv2.threshold(diff, threshold2, 255, cv2.THRESH_TOZERO_INV)
        _, thresh = cv2.threshold(thresh_1, threshold, 255, cv2.THRESH_BINARY)
    if thresh_m == 'darker_than_bg':
        diff = avg-src
        _, thresh_1 = cv2.threshold(diff, threshold2, 255, cv2.THRESH_TOZERO_INV)
        _, thresh = cv2.threshold(thresh_1, threshold, 255, cv2.THRESH_BINARY)
    if thresh_m == 'grab_cut':
        h,w = src.shape[:2]
        mask = np.zeros((h,w),dtype='uint8')
        #INITIALIZE rectangle:
        rect = (100,25,60,60)
        tmp1 = np.zeros((1, 13 * 5))
        tmp2 = np.zeros((1, 13 * 5))
        src_c = cv2.cvtColor(src,cv2.GRAY2RGB)
        cv2.grabCut(src_c,mask,rect,tmp1,tmp2,10,mode=cv2.GC_INIT_WITH_RECT)
        _, thresh = cv2.threshold(mask,2,255,cv2.THRESH_BINARY)
    if binaryops:
        kern_size = 2*int(binaryops*0.5)+1
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (kern_size,
                                                               kern_size))
        dilate = cv2.dilate(thresh,kernel)
        thresh = cv2.erode(dilate,kernel)
    return thresh

def get_PIX_Size(para_file):
    """get Pixel size info from para.ini file
    
    Parameters:
        para_file: *string*
            path to parameter file of experiment
            
    out: *float*
        Pixel size in micrometer
    """
    
    if os.path.exists(para_file):
        print 'Opening: '+ para_file
        with open(para_file, 'r') as ini_f:
            para_data = ini_f.read()
            pos = para_data.find('Pix Size')
            if (pos >= 0):
                PixSize_string= para_data[pos::].split(' = ')[1].split('\n')[0]
                PIX_SIZE = float(PixSize_string)
                print 'Pixel Size = ', PIX_SIZE
                
        return PIX_SIZE
    
    else:
        print ("No para.ini file found. Please add!\n"
               "PIX size set to 1.36 um")
        return 1.36

def get_FPS(cam_file):
    """get frame rate from camera.ini file
    
    Parameters:
        cam_file: *string*
            path to camera.ini file of experiment
            
    out: *int*
        Frame rate of measurement
    """

    if os.path.exists(cam_file):
        print 'Opening: '+ cam_file
        with open(cam_file, 'r') as ini_f:
            cam_data = ini_f.read()
            pos = cam_data.find('Frame Rate')
            if (pos >= 0):
                FPS_string= cam_data[pos::].split(' = ')[1].split('\n')[0]
                FPS = int(FPS_string)
                print 'FRAMERATE SET TO', FPS
        return FPS
    else:
        print ("No camera.ini file found. Please add!\n"
               "Frame rate set to 120")
        return 120

def PolarContour(contour, points=200, norm=True):
    '''Convert and interpolate xy-contour to polar coordinates r(phi) for 
    points angles. Returns distances and angles (r,phi) in Pixel values, if 
    norm = False.
    
    Parameters: 
        contour: *array_like*
            Array of shape (x,2) with stored xy - contour points.
        points: *int, optional*
            Number of points in the interpolated contour
        norm: *boolean, optional*
            True or False, norm contour values on mean radius
    
    Returns: *array_like, array_like*: distances_uniform, angles_uniform
        distances_uniform: array with points distances of the contour from 
        centroid to corresponding angles_uniform.
        angles_uniform: array with points angles, evenly distributed to describe
        contour
        
    Denpendencies: numpy, scipy.interpolate'''

    # TO AVOID ERRORS IN APPROXIMATION
    # IN POLAR SPACE
    xmin = min(contour[:,0])
    ymin = min(contour[:,1])
    if (xmin<0 or ymin<0):
        contour = [contour+(abs(xmin)+abs(ymin))]
        print 'contour was negative...'
    xmax = max(contour[:,0])
    ymax = max(contour[:,1])
    xymax = int(max(xmax,ymax))
    # Rebuild Contour to avoid compression at straight parts
    # there is one contour point at each pixel
    img = np.zeros([xymax+10,xymax+10],dtype='uint8')
    img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
    contour = np.array(contour, dtype = np.int32)
    cv2.drawContours(img,[contour],-1,(255,255,255),-1)
    img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    _,contour,_ = cv2.findContours(img,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
    # Do polar transform:
    M = cv2.moments(contour[0])
    cent_y = int(M['m10']/M['m00'])
    cent_x = int(M['m01']/M['m00'])
    x_vals = -(contour[0][:,0,1] -cent_x)
    #Mirror on x-axis..
    y_vals = (contour[0][:,0,0] -cent_y)
    # Convert coordinates to complex numbers
    # and do conversion to polar coordinates
    C = y_vals + 1j * x_vals
    angles = np.angle(C)
    distances = np.absolute(C)
    sortidx = np.argsort(angles)
    angles = angles[sortidx]
    distances = distances[sortidx]
    # wrap angles around. needed for interpolation
    angles = np.hstack(([ angles[-1] - 2*np.pi ], angles, [ angles[0] + 2*np.pi ]))
    distances = np.hstack(([distances[-1]], distances, [distances[0]]))
    if norm:
        distances /= np.mean(distances)
    # interpolation to evenly spaced angles
    # this is better for the fourier transform
    f = scipy.interpolate.interp1d(angles,distances)
    # take 200 points to get about 1pix resolution in case of an 60pix diameter cell
    angles_uniform = scipy.linspace(-np.pi,np.pi, num=points,endpoint=False)
    distances_uniform = f(angles_uniform)
    
    return distances_uniform, angles_uniform

def DFT_contour(contour,number_of_comp=101, points=200, norm=True):
    """Compute the (real) fast discrete fourier transform of an input contour.
    Returns the first number_of_comp coefficients of the DFT (complex array). 
    Contour points are converted to polar coordinates r(phi) for points angles.
    Contour can be reconstructed with numpy.fft.irfft(fft_coeffs). 
    
    Parameters: 
        contour: *array_like*
            Array of shape (x,2) with stored xy - contour points.
        number_of_comp: *int, optional*
            Order up to which fft coefficients should be returned.
        points: *int, optional*
            Number of points in the interpolated contour
        norm: *boolean, optional*
            True or False, norm contour values on mean radius
    
    Returns: 
        out: *complex ndarray*
            Array with DFT coefficients up to number_of_comp
            
    Remark: real fft only computes 1st half of the coeffiecients necessary for 
    reconstruction. The rest can be computed by taking the complex conjugate of 
    the Coeffs[1:] and adding them palindromic [a[0],a[1],a[2],...,
    conj(a[2]),conj(a[1])]  (numpy.flipud(numpy.conj(fft_coeffs[1:])))
    
    Dependencies: numpy, PolarContour()"""

    distances_uniform, _ = PolarContour(contour, points = points, norm = False)
    # do fft
    fft_coeffs = np.fft.rfft(distances_uniform)[:number_of_comp]

    if norm == True:
        fft_coeffs = fft_coeffs/fft_coeffs[0]
        
    return fft_coeffs

def extend_img(img, macro_block_size = 16):
    """Add black pixels at right and bottom border of image, so that 
    xy-size % macro_block_size = 0
    
    Parameters:
        img: *ndarray*
            2D or 3D np.array of image pixel values
        macro_block_size: *int*
            Integer value that image.size will be made dividable to
            
    out: *ndarray*
        img + black pixels at right and bottom border
    """
    if len(img.shape) == 2:
        x_size = img[0,:].size
        y_size = img[:,0].size
    if len(img.shape) == 3:
        x_size = img[0,:,0].size
        y_size = img[:,0,0].size    
     
    if x_size % macro_block_size == 0:
        right_add = 0
    else:
        right_add = ((x_size // macro_block_size + 1) * macro_block_size
                      - x_size)

    if y_size % macro_block_size == 0:
        bottom_add = 0
    else:
        bottom_add = ((y_size // macro_block_size + 1) * macro_block_size
                      - y_size)
 
    frame = cv2.copyMakeBorder(img, 0, bottom_add, 0, right_add, 
                               borderType = cv2.BORDER_CONSTANT)
    
    return frame

def GetVidData(video_file, detection_zone, avg_frames = 200,
               FirstFrame = 0, LastFrame = 0, write_img = False,
               write_video = False, save_dir = ".\\Offline"):    
    """Get contour data from video file.
    
    Parameters:
        video_file: *string*
            File path to video that will be analyzed
        detection_zone: *tuple or list, (x_min. x_max)*
            x_min and x_max in pixels where detection of objects is allowed 
        avg_frames: *int, optional*
            Number of frames taken as average for the background image
        FirstFrame: *int, optional*
            First frame in vidfile to analyze. Default is FirstFrame = 0.
        LastFrame: *int, optional*
            Last frame in vidfile to analyze. If LastFrame = 0 (default) all 
            video frames will be analyzed
        write_img: *boolean, optional*
            If true (default = False), backgroundimage will be written as png
        write_video: *boolean, optional*
            If true (default = False), two videos will be written: one showing 
            the detected contours and one showing the frames after thresholding
        save_dir: *string, optional*
            Directory where to save video files.
            
    Returns:
        out: *dictionary*
            DATA: Dictionary containg the contour parameters for every detected 
                event
    """

    in_file_split = "_".join(video_file.split("_")[:-1])
    tdmsfile = in_file_split + "_data.tdms"

    if not os.path.isfile(tdmsfile):
        tdmsfile = in_file_split + ".tdms"

    contours_file = in_file_split + "_contours.txt"
    print contours_file
    
    if os.path.exists(contours_file):
        print 'Opening: '+ contours_file
        with open(contours_file, 'r') as tfile:
            contour_data = tfile.readlines()
            for line in contour_data:
                if line.startswith('Contour in frame '):
                    line = line.strip('Contour in frame ')
                    line = line.strip(' \n')
                    first_frame_contours = int(line)
                    break
    else:
        print "No contours file found. Please add!"
        sys.exit(0)
    
    tdms_flag = os.path.isfile(tdmsfile)
    if tdms_flag:
        #check if tdms data and contourdata are aligned, if not --> aligne
        print "Opening TDMS file: " + str(tdmsfile)
        tdms_file = TdmsFile(tdmsfile)
        frame_tdms = (tdms_file.object('Cell Track', 'time')).data
        
        frame_skip_ind = np.nonzero(frame_tdms == first_frame_contours)[0][0]
        frame_tdms = frame_tdms[frame_skip_ind:]
    else:
        print ("No tdms file found. Please add to have time infromation "  
                "for the data. Data is written from video information only")

    in_video = cv2.VideoCapture(video_file)
    FRAMES = int(in_video.get(cv2.CAP_PROP_FRAME_COUNT))

    #in case the video file is shorter than avg_frames:
    if avg_frames > FRAMES:
        avg_frames = FRAMES

    if write_video:   
        if not os.path.isdir(save_dir):
            os.makedirs(save_dir)
    
        save_vidfile = os.path.join(save_dir, "test_contours_avi.avi")
        threshframe_vidfile = os.path.join(save_dir, "thresh_avi.avi")        
        test_vidwriter = imageio.get_writer(save_vidfile, fps=60, quality = 8,
                                            macro_block_size = macro_block_size)
        thresh_vidwriter = imageio.get_writer(threshframe_vidfile, fps=60, 
                                              quality = 8,
                                            macro_block_size = macro_block_size)
        
    #get info from para.ini file
    path, para_file = os.path.split(video_file)
    para_file = para_file.split('_')[0]+'_para.ini'
    para_file = os.path.join(path,para_file)
    PIX_SIZE = get_PIX_Size(para_file)
    
    #convert filter values to Pixels
    height_min = int(HEIGHT_MIN/PIX_SIZE)
    height_max = 2*int(HEIGHT_MAX/(2*PIX_SIZE))
    width_min = int(WIDTH_MIN/PIX_SIZE)
    width_max = 2*int(WIDTH_MAX/(2*PIX_SIZE))
    area_min = int(AREA_MIN/PIX_SIZE**2)
    area_max = int(AREA_MAX/PIX_SIZE**2)
    
    #create background image of channel
#    bg_img,_ = get_average(in_video, Start_Frame=FirstFrame, FRAMES=avg_frames)
    bg_img,_ = get_average(video_file, Start_Frame=FirstFrame, FRAMES=avg_frames)
    avg_img = np.float32(bg_img)

    #save data in dictionary
    DATA = {}
    
    if tdms_flag:
        keys = ["frame", "frame_tdms", "index", "area", "deformation", 
                "area ratio", "length", "height", "pos_x", "pos_y", 
                "inertia_ratio", "inertia_ratio_raw", "contour_raw", 
                "conv_hull"]
    else:
        keys = ["frame", "index", "area", "deformation", "area ratio",
                "length", "height", "pos_x", "pos_y", "inertia_ratio",
                "inertia_ratio_raw", "contour_raw", "conv_hull"]
        
    for key in keys:
        DATA[key] = []
    
    event_index = 0

    if LastFrame == 0 or LastFrame > FRAMES:
        LastFrame = FRAMES

    if FirstFrame < 0:
        FirstFrame = 0
        
    # Grab frame and continue while there is no error
    for frame_nr in xrange(FirstFrame, LastFrame):
        try:            
        # get frame from video
#            flag, frame = get_frame(in_video,frame_nr)
            frame = get_frame(video_file,frame_nr)
        
            #constantly update background image 
            if len(frame.shape) > 2:   
                gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            else:
                gray = frame            
            
            #update background image
            cv2.accumulateWeighted(gray, avg_img, 1./avg_frames)
            bg_img = cv2.convertScaleAbs(avg_img)
        
            proc_frame = img_proc(frame, bg_img, threshold = bg_threshold,
                                  thresh_m = 'darker_than_bg')  
        
            # Let OpenCV search for contours in the binary image
            # (Suzuki 1982)
            _,cont,_ = cv2.findContours(proc_frame,cv2.RETR_EXTERNAL,
                                        cv2.CHAIN_APPROX_NONE)
        
            #get rid of unnecessary 3rd dimension
            cont = [cont[i][:,0,:] for i in xrange(len(cont))]
            # Sort the returned list by length
            cont.sort(key=len)
            cont.reverse()
            
            hull = []
        
            if len(cont) > 0:
                for contour_index in xrange(len(cont)):
                    # get convexHull of contours. 
                    momments_raw = cv2.moments(cont[contour_index])
                    area_raw = momments_raw['m00']
                    Hull = cv2.convexHull(cont[contour_index],returnPoints=True)
                    # avoid single pixels taken into account
                    if len(Hull) > 1 and area_raw > 0:
                        #get rid of unnecessary 3rd dimension                
                        Hull = np.array(Hull[:,0,:], dtype = int)
                        #only append hull, if cell_size fits 
                        #in area_min < area(hull) < area_max 
                        M = cv2.moments(Hull)
                        area = M['m00']
                        #function expects a list as input
                        circ = circularity([Hull])  
                        defo = 1-circ
                        a_ratio = area / area_raw
                        #get bounding box
                        x,y,w,h = cv2.boundingRect(Hull)
        
                        pos_x = M['m10']/M['m00']
                        pos_y = M['m01']/M['m00']
        
                        inertia_ratio = np.sqrt(M["mu20"]/M["mu02"])
                        inertia_ratio_raw = np.sqrt(momments_raw["mu20"] / 
                                                    momments_raw["mu02"])
                        
                        # Check if the bounding box touches the edge of the ROI
                        # if so, skip to next contour
                        if (area > area_min and area < area_max and 
                            a_ratio < aratio_max and a_ratio > aratio_min and 
                            defo > def_min and defo < def_max and w > width_min 
                            and w < width_max and h > height_min and 
                            h < height_max and pos_x > detection_zone[0] 
                            and pos_x < detection_zone[1]):  
                            
                            hull.append(Hull)

                            if tdms_flag:
                                data = (frame_nr, frame_tdms[frame_nr],
                                        event_index, area, defo, a_ratio, w, h, 
                                        pos_x, pos_y, inertia_ratio, 
                                        inertia_ratio_raw, cont[contour_index],
                                        Hull)
                            else:
                                data = (frame_nr, event_index, area, defo,
                                        a_ratio, w, h, pos_x, pos_y,
                                        inertia_ratio, inertia_ratio_raw,
                                        cont[contour_index], Hull)
                                
                            for ind in xrange(len(data)):
                                DATA[keys[ind]].append(data[ind])
                                
                            event_index += 1     
    
                if write_video:                           
                    #write video to see how detection worked out
                    frame = cv2.cvtColor(frame, cv2.COLOR_GRAY2RGB)
                    cv2.drawContours(frame, hull, -1, (255,0,0))
                    frame = extend_img(frame)
                    test_vidwriter.append_data(frame)
                    proc_frame = extend_img(proc_frame)
                    proc_frame = cv2.cvtColor(proc_frame, cv2.COLOR_GRAY2RGB)
                    cv2.drawContours(proc_frame, hull, -1, (255,0,0))
                    thresh_vidwriter.append_data(proc_frame)
        except:
            print "Error while reading video at frame ", frame_nr
    
    if write_video:                                   
#        test_vid.release()
#        thresh_vid.release()
        test_vidwriter.close()
        thresh_vidwriter.close()
        cv2.destroyAllWindows()

    if write_img:
        cv2.imwrite(os.path.join(save_dir, "channel_img.png"), frame)    
            
    if DATA:
        return DATA
    else:
        print "No data was written. DATA = 0"
        return 0

def seperate_objects(DATA):
    """Assign object number to the events detecetd with GetVidData.
    
    Parameters:
        DATA: *dictionary*
            DATA dictionary from GetVidData
            
    Returns:
        out: *dictionary*
            Dictionary that contains info sorted for each object. keys are of 
            format 'Object 0', 'Object 1', ...
        """
    tdms_flag = False
    #find frames with more than 1 element/duplicates in frameNrs
    seen = {}
    duplicates = []
    for i in DATA['frame']:
        if i not in seen:
            seen[i] = 1
        else:
            if seen[i] == 1:
                duplicates.append(i)
            seen[i] += 1
    
    #create list with frames where only one object is in channel 
    frameNrsUniq = [value for value in DATA['frame'] if value not in duplicates]
    #Index of unique objects
    uniqIndex = [np.where(np.array(DATA['frame']) == value)[0] 
                    for value in frameNrsUniq]
    
    #get average velocity from single cell events only
    pos_xUniq = np.array(DATA['pos_x'])[uniqIndex]
    pos_xUniq = pos_xUniq[:,0] #indexing results in weird array shape
    
    pos_x_diff = np.diff(pos_xUniq) * -1 #object moving forward results in neg value
    #throw out all negative values --> here a new object appeared
    #only take displacement of consecutive events
    consecFrames = np.diff(frameNrsUniq)[pos_x_diff > 0]
    pos_x_diff = pos_x_diff[pos_x_diff > 0]
    pos_x_diff = pos_x_diff[consecFrames == 1]
    med_x_displacement = np.median(pos_x_diff)
    
    #cut off outliers in data for more reliable value
    #data = data within 1 sigma intervall
    pos_x_diff = pos_x_diff[pos_x_diff > med_x_displacement - np.std(pos_x_diff)]
    pos_x_diff = pos_x_diff[pos_x_diff < med_x_displacement + np.std(pos_x_diff)]
    med_x_displacement = np.median(pos_x_diff)
    
    #Uncertanty for the median displacement of the objects
    Delta_x_displacement = np.std(pos_x_diff)
    if Delta_x_displacement < WindowSize:
        Delta_x_displacement = WindowSize
        
    #pos_x[i] + (med_x_displacement +/- Delta_x_displacement) creates a zone where 
    #the obect from frame i is expected in frame i+1 
    
    Frames = np.array(DATA['frame'])
    Index = np.array(DATA['index'])
    Pos_x = np.array(DATA['pos_x'])
    
    if 'frame_tdms' in DATA.keys():
        tdms_flag = True
        TDMS_Frames = DATA['frame_tdms']
    
    #list that contains the frame number only once
    #set messes up the list for some reason --> sort
    frames = np.sort(list(set(Frames)))
    if tdms_flag:
        framesTDMS = np.sort(list(set(TDMS_Frames)))
    
    X_positions = []
    EventIndices = []
     
    #create list that contains all entries in each frame for pos_x and event_index
    if tdms_flag:
        framesIterator = framesTDMS
    else:
        framesIterator = frames

    for frame in framesIterator:
        if tdms_flag:
            ind = np.nonzero(TDMS_Frames == frame)
        else:
            ind = np.nonzero(frames == frame)
        X_positions.append(Pos_x[ind])    
        EventIndices.append(Index[ind])
    
    #dictionary to save object Data
    Objects = {}
    
    #create list that contains objects that should be kept track off 
    tracked_objects = [0]
    
    for frame_ind in xrange(len(framesIterator)):
    
        if frame_ind == 0:     
            stop_tracking = []
        else:
            for remove in stop_tracking:
                tracked_objects.remove(remove)
            stop_tracking = []
                
        for i in range(len(EventIndices[frame_ind])):
            if frame_ind == 0:
                key = "Object 0"
    #            ZoneKey = "Zone" + str(i)
                Ind = EventIndices[frame_ind][i]
                X = X_positions[frame_ind][i]
                if tdms_flag:
                    frame = framesTDMS[frame_ind]
                else:
                    frame = frames[frame_ind]
                Objects[key] = [(frame, Ind, X)]
                    
            else:
                Ind = EventIndices[frame_ind][i]
                X = X_positions[frame_ind][i]
                if tdms_flag:
                    frame = framesTDMS[frame_ind]
                else:
                    frame = frames[frame_ind]
    
                Found = False
                
                for tr_obj in tracked_objects:
                    key = "Object " + str(tr_obj)
    #                ZoneKey = "Zone" + str(tr_obj) 
                    prev_X =  Objects[key][-1][-1]
                    prev_frame = Objects[key][-1][0]

                    if len(Objects[key]) > 3:
                        #get object path so far
                        obj_X = np.array(Objects[key])[:,2]
                        obj_frames = np.array(Objects[key])[:,0]
                        
                        if len(obj_frames) == len(np.unique(obj_frames)):
                            displacement = (np.diff(obj_X) 
                                            // np.diff(obj_frames))
                            #For med_displacement don't take into account if
                            #cell got stuck at a constriction: displacement must
                            #be larger than 1!
                            displacement = displacement[displacement > 1]
                            if len(displacement) > 5:
                                med_displacement = np.median(displacement)
                    
                                predZoneStart = (prev_X - (frame - prev_frame)
                                                * med_displacement 
                                                - Delta_x_displacement)
                                predZoneEnd = (prev_X - (frame - prev_frame) 
                                              * med_displacement 
                                              + Delta_x_displacement)
                                prediction_zone = (predZoneStart,predZoneEnd)
                        
                            else:
                                predZoneStart = (prev_X - (frame - prev_frame) 
                                                * med_x_displacement 
                                                - Delta_x_displacement)
                                predZoneEnd = (prev_X - (frame - prev_frame)
                                               * med_x_displacement 
                                               + Delta_x_displacement)
                                prediction_zone = (predZoneStart,predZoneEnd)

                        else:
                            predZoneStart = (prev_X - (frame - prev_frame) 
                                            * med_x_displacement 
                                            - Delta_x_displacement)
                            predZoneEnd = (prev_X - (frame - prev_frame)
                                           * med_x_displacement 
                                           + Delta_x_displacement)
                            prediction_zone = (predZoneStart,predZoneEnd)
                            
                    else:
                        predZoneStart = (prev_X - (frame - prev_frame) 
                                        * med_x_displacement 
                                        - Delta_x_displacement)
                        predZoneEnd = (prev_X - (frame - prev_frame)
                                       * med_x_displacement 
                                       + Delta_x_displacement)
                        prediction_zone = (predZoneStart,predZoneEnd)

                    #check if prediction zone is outside the channel
                    if prediction_zone[1] < 0:
                        stop_tracking.append(tr_obj)
                        
                    else:                    
                        if X > prediction_zone[0] and X < prediction_zone[1]:
                            Objects[key].append((frame, Ind, X))
                            Found = True
                                                
                if Found == False:
                    new_obj_nr = len(Objects)
                    key = "Object " + str(new_obj_nr)
                    tracked_objects.append(new_obj_nr)
                    Objects[key] = [(frame, Ind, X)]
                    
                #list might contain same element twice --> removing procedure 
                #causes error --> get rid of duplicates
                stop_tracking = list(set(stop_tracking))
    
    return Objects

def get_ObjectData(Objects, DATA):
    """Assign parameters to the Objects dictionary determined before.

    Parameters:
        Objects: *dictionary*
            Objects dictionary from fct seperate_objects
        DATA: *dictionary*
            DATA dictionary with video data from GetVidData
    Returns:
        out: *dictionary*, *dictionary*
            DATA, ObjectsData\n
            DATA: dict like DATA with assigned parameters for every event
            ObjectsDATA: dict where data is sorted for each object, keys are of 
            format 'Object 0', 'Object 1', ...
    """
    #Create new dictionary where parameters are assigned for every object and 
    #frame
    ObjectsData = {}
        
    object_index = np.zeros_like(DATA['frame'])
    
    for j in xrange(len(Objects)):
        key = 'Object ' + str(j)
        objectdata = {}
        ind = np.array(Objects[key], dtype = int)[:,1]
        
        for DataKey in DATA:
            objectdata[DataKey] = np.array(DATA[DataKey])[ind]
            
        ObjectsData[key] = objectdata
        object_index[ind] = j
    
    DATA['object index'] = object_index
    
    return DATA, ObjectsData

def filter_ObjectsDATA(ObjectsData_dict, DATA, detection_zone):
    """Filter the ObjectsData dictionary for unwanted events: 2 objects in same
    prediction zone; Object path not from beginning to end. Filtered objects 
    will be deleted from ObjectsData_dict.
    
    Parameters: 
        ObjectsData_dict: *dictionary*
            Dictionary containing objects data acquired with get_ObjectData().
            Filtered objects will be deleted from this dictionary.
        DATA: *dictionary*
            DATA dictionary with video data from GetVidData
    
    out: *dictionary*
        DATA_filtered
            DATA dictionary filtered for unwanted events
    """
    #filter the Objectsdata dict for bad events
    del_keys = []
    DATA_filtered = DATA.copy()

    for key in ObjectsData_dict.keys():
        obj_nr = int(key.split(" ")[-1])
        obj_ind = np.nonzero(DATA_filtered['object index'] == obj_nr)[0]

        #if more events where written to ObjectsData then available in DATA, 
        #something went wrong while data writing 
        if (len(np.array(DATA['frame'])[obj_ind]) 
            != len(ObjectsData_dict[key]['frame'])):
            
            del_keys.append(key)
        
        #exclude two ore more objects in same pred_zone
        if (len(np.unique(ObjectsData_dict[key]['frame'])) 
                != len(ObjectsData_dict[key]['frame'])):
            
            del_keys.append(key)
            
        #measured cells must make it from channel entrance to exit
        if (ObjectsData_dict[key]['pos_x'][0] < detection_zone[1] - 100 
            or ObjectsData_dict[key]['pos_x'][-1] 
                > detection_zone[0] + 100): 
    
            del_keys.append(key)
        
    for key in set(del_keys):
        ObjectsData_dict.pop(key)
        
        obj_nr = int(key.split(" ")[-1])
        obj_ind = np.nonzero(DATA_filtered['object index'] == obj_nr)[0]
                    
        for datakey in DATA.keys():
            DATA_filtered[datakey] = np.delete(np.array(DATA_filtered[datakey]),
                                                 obj_ind)

    return DATA_filtered
    
def get_VelocityAndTime(ObjectsData, DATA, detection_zone, FPS, PIX_Size):
    """Add velocity and time information to the ObjectsData dictionary and DATA
    dict. For every object velocity[0] = velocity[1] and time[0] is determined 
    by velocity[0] and the distance of the object to channel entry. 
    
    Parameters:
        ObjectsData: *dictionary*
            Dictionary containing objects data acquired with get_ObjectData().
            Filtered objects will be deleted from this dictionary.
        DATA: *dictionary*
            DATA dictionary with video data from GetVidData
    
    out: *dictionary*
        DATA
            DATA dictionary with added velocity and time info
    """
    
    DATA_index = DATA['index']
    DATA['velocity'] = np.zeros(len(DATA_index))
    DATA['time'] = np.zeros(len(DATA_index))

    if 'frame_tdms' in DATA.keys():
        tdms_flag = True
    else:
        tdms_flag = False
        
    for key in ObjectsData.keys():
        
        pos_x = ObjectsData[key]['pos_x']
        
        if tdms_flag:
            frames = ObjectsData[key]['frame_tdms']
        else:
            frames = ObjectsData[key]['frame']
        
        velocity = np.zeros_like(frames)
        time = np.zeros_like(frames)
        
        Delta_frames = np.diff(frames)
        if len(Delta_frames) > 1: 

            #velocity in um/s
            velocity[1:] = abs(np.diff(pos_x)) * PIX_Size / Delta_frames * FPS 
            velocity[0] = velocity[1]
            
            time0 = (detection_zone[1] - pos_x[0]) * PIX_Size / velocity[0]
            time = (frames - frames[0]) / float(FPS) + time0
            
            #velocity in mm/s
            ObjectsData[key]['velocity'] = velocity / 1000.
            ObjectsData[key]['time'] = time
    
            #add DATA to DATA dict
            index = ObjectsData[key]['index']   
            for ii in range(len(index)):
                ind = np.nonzero(DATA_index == index[ii])[0]
                DATA['velocity'][ind] = velocity[ii] / 1000.
                DATA['time'][ind] = time[ii]    

def sort_DATA_by_objNr(DATA):
    """Sort the data in the DATA dictionary so that it is arranged by objects nr
    instead of event index.

    Parameters:
        DATA: *dictionary*
            DATA dictionary with video data from GetVidData
    
    out: *dictionary*
        DATA
            DATA dictionary with added velocity and time info    
    """

    obj_nrs = DATA['object index']
    index = np.arange(len(obj_nrs))

    #get unique list of all object indices    
    obj_nrs_uniq = np.sort(list(set(obj_nrs)))
    index_new = []
    
    for ii in obj_nrs_uniq:
        ind_obj = np.nonzero(obj_nrs == ii)[0]
        #find the indeces assigned to the object nr and sort
        index_obj = np.sort(index[ind_obj])
        index_new.append(index_obj)
            
    #get 1D-array from list of lists 
    index_new = np.array(list(chain.from_iterable(index_new)))
    
    ind = np.nonzero(index == index_new)[0]
    
    #use new indexing on all data in DATA
    returnDATA = {}
    for key in DATA.keys():
        returnDATA[key] = np.array(DATA[key])[ind]
        
    return returnDATA
    
def VideoWriteSepObjects(in_video, DATA, FirstFrame = 0, LastFrame = 0,
                         save_vidfile = os.path.join(VideoDir,
                                                       "sepCells_avi.avi")):
    """Write video showing which object number was assigned to each detected 
    event.
    Parameters:
        in_video: *string*
            File path to video file that was analyzed and to which contours 
            will be drawn.
        DATA: *dictionary*
            Dictionary containing the object information for each videoframe
        FirstFrame: *int, optional*
            First frame in vidfile to analyze. Default is FirstFrame = 0.
        LastFrame: *int, optional*
            Last frame in vidfile to analyze. If LastFrame = 0 (default) all 
            video frames will be analyzed
        save_vidfile: *string, optional*
            File path where finished video will be saved. If not specified, the
            default path is ".\Offline\test_avi.avi" 
    """

    if not os.path.isdir(VideoDir):
        os.makedirs(VideoDir)

    videoReader = imageio.get_reader(in_video)
    FRAMES = videoReader.get_meta_data()['nframes']
    
    vidwriter = imageio.get_writer(save_vidfile, fps=60, quality = 8,
                                   macro_block_size = macro_block_size)
    
    #default colors of matplotlib. * 255 to concert to uint8 for use with cv2
#    colors = np.array(plt.cm.get_cmap('tab10').colors) * 255
    colors = np.array(cm.get_cmap('tab10').colors) * 255
    contourData = np.array(DATA['conv_hull'])
    object_index = DATA['object index']
    object_index = np.array(object_index, dtype = int)
    Pos_x = np.array(DATA['pos_x'])
    Pos_y = np.array(DATA['pos_y'])
    font = cv2.FONT_HERSHEY_SIMPLEX
    
    if LastFrame == 0 or LastFrame > FRAMES:
        LastFrame = FRAMES

    if FirstFrame < 0:
        FirstFrame = 0
        
    for frame_nr in xrange(FirstFrame, LastFrame):        
    
        try:
            #get frame from video
            frame = videoReader.get_data(frame_nr)
            
            #index of events found in this frame 
            Frames = np.array(DATA['frame'])
            ind = np.nonzero(Frames == frame_nr)
            contours = contourData[ind]
            objects = object_index[ind]
            pos_x = Pos_x[ind]
            pos_y = Pos_y[ind]

            #write video to see how detection worked out
            if len(ind[0])>0:
                for cont in xrange(len(ind[0])):
                    obj_ind = objects[cont]

                    cv2.drawContours(frame, [contours[cont]], -1,
                                     colors[obj_ind % len(colors)], -1)
                    cv2.putText(frame, str(obj_ind), 
                                (int(pos_x[cont])-5,int(pos_y[cont])),
                                font, 0.5, (255,255,255),2,cv2.LINE_AA)
            
            frame = extend_img(frame)
            vidwriter.append_data(frame)
           
        except:
            print "Error while reading video at frame ", frame_nr

    videoReader.close()
    vidwriter.close()

def addDFT_DataToDict(Dict, no_DFTcoeffs = 10, norm = True):
    """Add the DFT coeffients to the DATA dictionary.
    
    Parameters: 
        Dict: *dictionary*
            Dictionary that contains the data arrays determined by 
            get_ObjectData
        no_DFTcoeffs: *int, optional*
            Max order of the DFT that will be returned.
        norm: *boolean, optional*
            If true (default) the returned coefficients are normalized on the 
            zero frequency order.
            
        Returns:
            out: *dictionary*
                DATA dictionary with added key values: 
                Dict['DFT_coeffs'][i] = DFT_contour
    """
                
    #array to store DFT coeffs
    ContDFT_coeffs = np.zeros((no_DFTcoeffs,len(Dict['index']))) * (0 + 0j)
    #"index" starts at 1
    for i in range(len(Dict['index'])):
        contour = Dict['conv_hull'][i]
        ContDFT_coeffs[:,i] = DFT_contour(contour,number_of_comp = no_DFTcoeffs, 
                                     points = 200, norm = norm)
        
    Dict['DFT_coeffs'] = ContDFT_coeffs
    
    return Dict

def DataToTSV(Dict,tsv_path):
    """Write the Data from the DATA dictionary to a .tsv file. Does not include
    contour data!
    
    Parameters:     
        Dict: *dictionary*
            Dictionary that contains the data arrays determined by 
            get_ObjectData
        tsv_path: *string*
            File path where results file will be saved.
    """
    
    header = "\t".join([key for key in Dict.keys() if not (key == 'DFT_coeffs' 
                        or key == 'contour_raw' or key == 'conv_hull')])
                        
    data = [np.array(Dict[key]) for key in Dict.keys() 
            if not (key == 'DFT_coeffs' or key == 'contour_raw' 
                                        or key == 'conv_hull')]

    #if     
    if 'DFT_coeffs' in Dict.keys():
        #save real and imaginary part separated by column for each harmonic
        for ind, c in zip(range(len(Dict['DFT_coeffs'])),Dict['DFT_coeffs']):
            data.append(c.real)
            header += "\tDFT_coeff_real_" + str(ind) 
            data.append(c.imag)
            header += "\tDFT_coeff_imag_" + str(ind)
    
    data = np.array(data).transpose()
    
    with open(tsv_path, "w") as tfile:
        # write header
        tfile.write("# "+header+"\n")
    #with open(tsv_path, 'ab') as tfile:
        np.savetxt(tfile, data, delimiter = "\t", fmt = '%.8f')

if __name__ == '__main__':

#    in_video = cv2.VideoCapture(in_file)
#    _, img = get_frame(in_video,first_frame)
    img = get_frame(in_file,first_frame)
    
    img_resize = 1.
    rect_pts = define_rect(img, resize = img_resize)

    detection_zone = [rect_pts[0][0]/img_resize, rect_pts[1][0]/img_resize]
    detection_zone.sort()

    #get info from para.ini file
    path, para_file = os.path.split(in_file)
    para_file = para_file.split('_')[0]+'_para.ini'
    para_file = os.path.join(path,para_file)
    PIX_SIZE = get_PIX_Size(para_file)
    
    #Get frame rate info from camera.ini file
    path, cam_file = os.path.split(in_file)
    cam_file = cam_file.split('_')[0]+'_camera.ini'
    cam_file = os.path.join(path,cam_file)
    FPS = get_FPS(cam_file)
    
    DATA = GetVidData(in_file, detection_zone, FirstFrame = first_frame,
                      LastFrame = last_frame, write_video = False)
    
    Objects = seperate_objects(DATA)
    DATA, ObjectsData = get_ObjectData(Objects, DATA)

#    DATA = get_VelocityAndTime(ObjectsData, DATA, detection_zone, FPS, PIX_SIZE)
    get_VelocityAndTime(ObjectsData, DATA, detection_zone, FPS, PIX_SIZE)
    
    DATA_filtered = filter_ObjectsDATA(ObjectsData, DATA, detection_zone)

    DATA_filtered = sort_DATA_by_objNr(DATA_filtered)
    
#    VideoWriteSepObjects(in_file, DATA_filtered, FirstFrame = first_frame, 
#                         LastFrame = last_frame)

#    DATA = addDFT_DataToDict(DATA, no_DFTcoeffs = 10)
#    
    DataToTSV(DATA_filtered,tsv_name)
